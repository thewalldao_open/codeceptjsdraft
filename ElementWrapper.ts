
import { expect, ElementHandle, Locator, LocatorScreenshotOptions } from '@playwright/test';
import BrowserWrapper from './BrowserWrapper';
import { CustomLocator } from './struct';


class ElementWrapper extends BrowserWrapper {
  /**
   * Get locator by CustomLocator argument
   */
  public getLocator(locator: CustomLocator): Locator {
    let actualSelector: string;

    if (typeof (locator.selector) != "string") {
      actualSelector = locator.selector["value"];
    } else {
      actualSelector = locator.selector;
    }

    if (locator.frameSelector != undefined) {
      let frameLocator = this.page.frameLocator(locator.frameSelector[0]);
      for (let i = 1; i < locator.frameSelector.length; i++) {
        frameLocator = frameLocator.frameLocator(locator.frameSelector[i]);
      }
      return frameLocator.locator(actualSelector)
    } else {
      return this.page.locator(actualSelector)
    }
  }

  /**
   * Wait for all element of locator visible on web
   */
  public async waitForAllElementVisibleOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await this.getLocator(locator).elementHandles()).length) > 0
          && (await this.getLocator(locator).elementHandles()).every(async ele => await ele.isVisible());
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more elements are still hidden: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Wait for all text of locator present on web
   */
  public async waitForAllTextPresentOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await this.getLocator(locator).elementHandles()).length) > 0
          && (await this.getLocator(locator).elementHandles()).every(async ele => (await ele.textContent()) != "");
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more text are still not present: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Wait for all innertext of locator present on web
   */
  public async waitForAllInnerTextPresentOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await this.getLocator(locator).elementHandles()).length) > 0
          && (await this.getLocator(locator).elementHandles()).every(async ele => (await ele.innerText()) != "");
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more innertext are still not present: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Wait for all element of locator attached on web
   */
  public async waitForAllElementAttachedOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await this.getLocator(locator).elementHandles()).length) > 0;
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more elements are still Detached: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Wait for all element of locator hidden on web
   */
  public async waitUntilAllElementHiddenOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await (this.getLocator(locator)).elementHandles()).length) <= 0
          && (await (this.getLocator(locator)).elementHandles()).every(async ele => !(await ele.isHidden()));
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more elements are still visible: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Wait for all element of locator detached on web
   */
  public async waitUntilAllElementDetachedOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await (this.getLocator(locator)).elementHandles()).length) <= 0
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nOne or more elements are still Attached: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * Returns when element specified by locator satisfies the `state` option.
   *
   * If target element already satisfies the condition, the method returns immediately. Otherwise, waits for up to `timeout`
   * milliseconds until the condition is met.
   *
   * ```js
   * const orderSent = page.locator('#order-sent');
   * await orderSent.waitFor();
   * ```
   *
   * @param options
   */
  public async waitFor(locator: CustomLocator, state: "attached" | "detached" | "visible" | "hidden" = "visible", timeout: number = this.actionTimeout): Promise<void> {
    try {
      await this.getLocator(locator).waitFor({ state: state, timeout: timeout });
    } catch (error) {
    }
  }

  /**
   * choose wait for all element style
   */
  public async chooseWaitAllStyle(locator: CustomLocator, state: "attached" | "detached" | "visible" | "hidden" | "textPresent" | "innertextPresent" = "visible", timeout: number = this.actionTimeout): Promise<void> {
    switch (state) {
      case "attached":
        await this.waitForAllElementAttachedOnWeb(locator, timeout)
        break;
      case "detached":
        await this.waitUntilAllElementDetachedOnWeb(locator, timeout)
        break;
      case "visible":
        await this.waitForAllElementVisibleOnWeb(locator, timeout)
        break;
      case "hidden":
        await this.waitUntilAllElementHiddenOnWeb(locator, timeout)
        break;
      case "textPresent":
        await this.waitForAllTextPresentOnWeb(locator, timeout)
        break;
      case "innertextPresent":
        await this.waitForAllInnerTextPresentOnWeb(locator, timeout)
        break;
      default:
        await this.waitForAllElementVisibleOnWeb(locator, timeout)
        break;
    }
  }

  /**
   * choose wait for an element style
   */
  public async chooseWaitStyle(locator: CustomLocator, state: "attached" | "detached" | "visible" | "hidden" | "textPresent" | "innertextPresent" = "visible", timeout: number = this.actionTimeout): Promise<void> {
    switch (state) {
      case "attached":
        await this.waitFor(locator, "attached", timeout);
        break;
      case "detached":
        await this.waitFor(locator, "detached", timeout)
        break;
      case "visible":
        await this.waitFor(locator, "visible", timeout)
        break;
      case "hidden":
        await this.waitFor(locator, "hidden", timeout)
        break;
      case "textPresent":
        await this.waitForTextPresentOnWeb(locator, timeout)
        break;
      case "innertextPresent":
        await this.waitForInnerTextPresentOnWeb(locator, timeout)
        break;
      default:
        await this.waitFor(locator, "visible", timeout)
        break;
    }
  }

  /**
   * Resolves given locator to the first matching DOM element. If no elements matching the query are visible, waits for them
   * up to a given timeout. If multiple elements match the selector, throws.
   */
  public async elementHandle(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<ElementHandle<SVGElement | HTMLElement> | null> {
    return await this.getLocator(locator).elementHandle({ timeout: timeout })
  }

  /**
   * Resolves given locator to all matching DOM elements.
   */
  public async elementHandles(locator: CustomLocator, waitState: "attached" | "detached" | "visible" | "hidden" = "attached", timeout: number = this.actionTimeout): Promise<ElementHandle<Node>[]> {
    await this.chooseWaitAllStyle(locator, waitState, timeout);
    return await this.getLocator(locator).elementHandles()
  }

  /**
   * Returns the number of elements matching given locator.
   */
  public async count(locator: CustomLocator, waitState: "attached" | "detached" | "visible" | "hidden" = "attached", timeout: number = this.actionTimeout): Promise<number> {
    await this.chooseWaitAllStyle(locator, waitState, timeout);
    return await this.getLocator(locator).count()
  }

  /**
   * Returns the `node.textContent`.
   */
  public async getText(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<string | null> {
    return await this.getLocator(locator).textContent({ timeout: timeout })
  }

  /**
   * Returns `input.value` for the selected `<input>` or `<textarea>` or `<select>` element.
   *
   * Throws for non-input elements. However, if the element is inside the `<label>` element that has an associated
   * [control](https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement/control), returns the value of the control.
   */
  public async getValue(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<string | null> {
    return await this.getLocator(locator).inputValue({ timeout: timeout })
  }

  /**
   * Returns the `element.innerText`.
   */
  public async getInnerText(locator: CustomLocator, timeout = this.actionTimeout): Promise<string | null> {
    return await this.getLocator(locator).innerText({ timeout: timeout })
  }

  /**
   * Returns an array of `node.textContent` values for all matching nodes.
   */
  public async getAllText(locator: CustomLocator, waitState: "attached" | "visible" | "textPresent" = "visible", timeout: number = this.actionTimeout): Promise<string[]> {
    await this.chooseWaitAllStyle(locator, waitState, timeout);
    return await this.getLocator(locator).allTextContents()
  }

  /**
   * Returns an array of `node.inputValue` values for all matching nodes.
   */
  public async getAllValues(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<string[]> {
    const elements = await this.elementHandles(locator, "attached", timeout);
    const texts: string[] = await Promise.all(elements.map(async (item): Promise<string> => {
      return (await item.innerText())!;
    }));
    return texts;
  }

  /**
   * Returns an array of `node.innerText` values for all matching nodes.
   */
  public async getAllInnerText(locator: CustomLocator, waitState: "attached" | "visible" | "innertextPresent" = "attached", timeout: number = this.actionTimeout): Promise<string[]> {
    await this.chooseWaitAllStyle(locator, waitState, timeout);
    return await this.getLocator(locator).allInnerTexts()

  }

  /**
   * Returns the `element.innerHTML`.
   */
  public async getInnerHTML(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<string | null> {
    // await this.waitFor(locator, "attached")
    return await this.getLocator(locator).innerHTML({ timeout: timeout })
  }

  /**
   * Returns the computed style value
   */
  public async getCssValue(locator: CustomLocator, value: any, timeout: number = this.actionTimeout): Promise<string> {
    this.waitFor(locator, "attached", timeout);
    const computedStyle: CSSStyleDeclaration = await this.getLocator(locator).evaluate(element => {
      return getComputedStyle(element)
    })
    return computedStyle[value];
  }

  /**
   * Returns an array of the computed style value
   */
  public async getAllCssValue(locator: CustomLocator, value: string, timeout: number = this.actionTimeout): Promise<string[]> {
    this.chooseWaitAllStyle(locator, "attached", timeout)
    const computedStyleList: CSSStyleDeclaration[] = await this.getLocator(locator).evaluateAll(list => list.map(element => {
      return getComputedStyle(element)
    }))
    const valueList = computedStyleList.map(computedStyle => computedStyle[value])
    return valueList;
  }

  /**
   * Returns element attribute value.
   */
  public async getAttribute(locator: CustomLocator, attribute: string, timeout: number = this.actionTimeout): Promise<string> {
    return (await this.getLocator(locator).getAttribute(attribute, { timeout: timeout }))!;
  }

  /**
   * Returns an array of element attribute value.
   */
  public async getAllAttributes(locator: CustomLocator, attribute: string, timeout: number = this.actionTimeout): Promise<string[]> {
    const elements = await this.elementHandles(locator, "attached", timeout);
    const texts: string[] = await Promise.all(elements.map(async (item): Promise<string> => {
      return (await item.getAttribute(attribute))!;
    }));
    return texts;
  }

  /**
   * This method clicks the element by performing the following steps:
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element, or
   *    the specified `position`.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   * @param options
   */
  public async clickOnWeb(locator: CustomLocator,
    options: {
      button?: "left" | "right" | "middle";
      clickCount?: number;
      delay?: number;
      force?: boolean;
      modifiers?: ("Alt" | "Control" | "Meta" | "Shift")[];
      noWaitAfter?: boolean;
      position?: {
        x: number;
        y: number;
      };
      timeout?: number;
      trial?: boolean;
    } = { timeout: this.actionTimeout }) {
    await this.getLocator(locator).click(options)
  }

  /**
   * This method clicks the element by performing the following steps:
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element, or
   *    the specified `position`.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   * @param options
   */
  public async rightClickOnWeb(locator: CustomLocator, options: {
    button?: "right";
    clickCount?: number;
    delay?: number;
    force?: boolean;
    modifiers?: ("Alt" | "Control" | "Meta" | "Shift")[];
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }) {
    options = {
      button: "right"
    }
    await this.getLocator(locator).click(options);
  }

  /**
   * This method clicks the element by performing the following steps:
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element, or
   *    the specified `position`.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   * @param options
   */
  public async middleClickOnWeb(locator: CustomLocator, options: {
    button?: "middle";
    clickCount?: number;
    delay?: number;
    force?: boolean;
    modifiers?: ("Alt" | "Control" | "Meta" | "Shift")[];
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }) {
    options = {
      button: "middle"
    }
    await this.getLocator(locator).click(options);
  }

  /**
   * This method double clicks the element by performing the following steps:
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to double click in the center of the
   *    element, or the specified `position`.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set. Note that if the
   *    first click of the `dblclick()` triggers a navigation event, this method will throw.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   *
   * > NOTE: `element.dblclick()` dispatches two `click` events and a single `dblclick` event.
   * @param options
   */
  public async doubleClick(locator: CustomLocator, options: {
    button?: "left" | "right" | "middle";
    delay?: number;
    force?: boolean;
    modifiers?: Array<"Alt" | "Control" | "Meta" | "Shift">;
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }) {
    await this.getLocator(locator).dblclick(options);
  }

  /**
    * This method checks the element by performing the following steps:
    * 1. Ensure that element is a checkbox or a radio input. If not, this method throws. If the element is already checked,
    *    this method returns immediately.
    * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
    * 1. Scroll the element into view if needed.
    * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element.
    * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
    * 1. Ensure that the element is now checked. If not, this method throws.
    *
    * If the element is detached from the DOM at any moment during the action, this method throws.
    *
    * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
    * zero timeout disables this.
    * @param options
    */
  public async check(locator: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).check(options)
  }

  /**
   * This method unchecks the element by performing the following steps:
   * 1. Ensure that element is a checkbox or a radio input. If not, this method throws. If the element is already
   *    unchecked, this method returns immediately.
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
   * 1. Ensure that the element is now unchecked. If not, this method throws.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   * @param options
   */
  public async uncheck(locator: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).uncheck(options)
  }

  /**
    * This method checks the element by performing the following steps:
    * 1. Ensure that element is a checkbox or a radio input. If not, this method throws. If the element is already checked,
    *    this method returns immediately.
    * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
    * 1. Scroll the element into view if needed.
    * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element.
    * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
    * 1. Ensure that the element is now checked. If not, this method throws.
    *
    * If the element is detached from the DOM at any moment during the action, this method throws.
    *
    * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
    * zero timeout disables this.
    * @param options
    */
  public async checkAll(locator: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.shortTimeout }): Promise<void> {
    const elements = await this.elementHandles(locator, "visible", this.actionTimeout);
    elements.forEach(async element => {
      await element.check(options)
    });
  };


  /**
   * This method unchecks the element by performing the following steps:
   * 1. Ensure that element is a checkbox or a radio input. If not, this method throws. If the element is already
   *    unchecked, this method returns immediately.
   * 1. Wait for [actionability](https://playwright.dev/docs/actionability) checks on the element, unless `force` option is set.
   * 1. Scroll the element into view if needed.
   * 1. Use [page.mouse](https://playwright.dev/docs/api/class-page#page-mouse) to click in the center of the element.
   * 1. Wait for initiated navigations to either succeed or fail, unless `noWaitAfter` option is set.
   * 1. Ensure that the element is now unchecked. If not, this method throws.
   *
   * If the element is detached from the DOM at any moment during the action, this method throws.
   *
   * When all steps combined have not finished during the specified `timeout`, this method throws a [TimeoutError]. Passing
   * zero timeout disables this.
   * @param options
   */
  public async uncheckAll(locator: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    position?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.shortTimeout }): Promise<void> {
    const elements = await this.elementHandles(locator, "visible", this.actionTimeout);
    elements.forEach(async element => {
      await element.uncheck(options)
    });
  };

  /**
   * Returns whether the element is checked. Throws if the element is not a checkbox or radio input.
   */
  public async checkIfTheElementIsCheckedOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitForElementCheckedOnWeb(locator, timeOutInsecond)
      return await this.getLocator(locator).isChecked();
    } catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * Returns whether the element is disabled, the opposite of [enabled](https://playwright.dev/docs/actionability#enabled).
   */
  public async checkIfTheElementIsDisabledOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitUntilElementDisabledOnWeb(locator, timeOutInsecond)
      return await this.getLocator(locator).isDisabled({ timeout: 1000 });
    }
    catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * Returns whether the element is [editable](https://playwright.dev/docs/actionability#editable).
   */
  public async checkIfTheElementIsEditableOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitForElementEditableOnWeb(locator, timeOutInsecond)
      return await this.getLocator(locator).isEditable({ timeout: 1000 });
    }
    catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * Returns whether the element is [enabled](https://playwright.dev/docs/actionability#enabled).
   */
  public async checkIfTheElementIsEnabledOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitForElementEnableOnWeb(locator, timeOutInsecond)
      return await this.getLocator(locator).isEnabled({ timeout: 1000 });
    }
    catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * Returns whether the element is hidden, the opposite of [visible](https://playwright.dev/docs/actionability#visible).
   */
  public async checkIfTheElementIsHiddenOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitFor(locator, "hidden", timeOutInsecond)
      return await this.getLocator(locator).isHidden({ timeout: 1000 });
    }
    catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * Returns whether the element is [visible](https://playwright.dev/docs/actionability#visible).
   */
  public async checkIfTheElementIsVisibleOnWeb(locator: CustomLocator, timeOutInsecond: number = this.isTimeout): Promise<boolean> {
    try {
      await this.waitFor(locator, "visible", timeOutInsecond)
      return await this.getLocator(locator).isVisible({ timeout: 1000 });
    } catch (error) {
      console.log(error.message)
      return false;
    }
  }

  /**
   * wait for editable element has text or to a DOM node that has text on web.
   */
  public async waitForTextPresentOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).not.toBeEmpty({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element visible on web.
   */
  public async waitForElementDisplayOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeVisible({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait until element disappear on web.
   */
  public async waitUntilElementDisappearOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeHidden({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element editable on web.
   */
  public async waitForElementEditableOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeEditable({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element enable on web.
   */
  public async waitForElementEnableOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeEnabled({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait Until element disabled on web.
   */
  public async waitUntilElementDisabledOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeDisabled({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element checked on web.
   */
  public async waitForElementCheckedOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toBeChecked({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element unchecked on web.
   */
  public async waitUntilElementUncheckedOnWeb(locator: CustomLocator, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).not.toBeChecked({ timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element contain text.
   */
  public async waitForElementOnWebContainText(locator: CustomLocator, text: string | RegExp | (string | RegExp)[], timeOutInsecond: number = this.actionTimeout, useInnerText: boolean = false): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toContainText(text, { timeout: timeOutInsecond, useInnerText: useInnerText })
    } catch (error) {
    }
  }

  /**
   * wait for element to gave text.
   */
  public async waitForElementOnWebToHaveText(locator: CustomLocator, text: string | RegExp | (string | RegExp)[], timeOutInsecond: number = this.actionTimeout, useInnerText: boolean = false): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toHaveText(text, { timeout: timeOutInsecond, useInnerText: useInnerText })
    } catch (error) {
    }
  }

  /**
   * wait for element to gave value.
   */
  public async waitForElementOnWebToHaveValue(locator: CustomLocator, value: string | RegExp, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toHaveValue(value, { timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for element to gave css value.
   */
  public async waitForElementOnWebToHaveCss(locator: CustomLocator, name: string, value: string | RegExp, timeOutInsecond: number = this.actionTimeout): Promise<void> {
    try {
      await expect(this.getLocator(locator)).toHaveCSS(name, value, { timeout: timeOutInsecond })
    } catch (error) {
    }
  }

  /**
   * wait for innerText of element Present on web.
   */
  public async waitForInnerTextPresentOnWeb(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await expect.poll(async () => {
        return ((await this.getLocator(locator).elementHandles()).length) > 0
          && ((await this.getLocator(locator).innerText()) != "");
      }, {
        timeout: timeout
      }).toBeTruthy()
    } catch (error) {
      console.error(`Condition is false:\nInnerText is still not present: ${this.getLocator(locator)} with timeout ${timeout}`)
    }
  }

  /**
   * This method waits for [actionability](https://playwright.dev/docs/actionability) checks, focuses the element, fills it and triggers an `input`
   * event after filling. Note that you can pass an empty string to clear the input field.
   *
   * If the target element is not an `<input>`, `<textarea>` or `[contenteditable]` element, this method throws an error.
   * However, if the element is inside the `<label>` element that has an associated
   * [control](https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement/control), the control will be filled
   * instead.
   *
   * To send fine-grained keyboard events, use
   * [locator.type(text[, options])](https://playwright.dev/docs/api/class-locator#locator-type).
   */
  public async clear(locator: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    timeout: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).fill("", options);
  }

  /**
    * Focuses the element, and then sends a `keydown`, `keypress`/`input`, and `keyup` event for each character in the text.
    *
    * To press a special key, like `Control` or `ArrowDown`, use
    * [locator.press(key[, options])](https://playwright.dev/docs/api/class-locator#locator-press).
    *
    * ```js
    * await element.type('Hello'); // Types instantly
    * await element.type('World', {delay: 100}); // Types slower, like a user
    * ```
    *
    * An example of typing into a text field and then submitting the form:
    *
    * ```js
    * const element = page.locator('input');
    * await element.type('some text');
    * await element.press('Enter');
    * ```
    */
  public async type(locator: CustomLocator, value: string, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    timeout: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).type(value, options);
  }

  /**
   * This method waits for [actionability](https://playwright.dev/docs/actionability) checks, focuses the element, fills it and triggers an `input`
   * event after filling. Note that you can pass an empty string to clear the input field.
   *
   * If the target element is not an `<input>`, `<textarea>` or `[contenteditable]` element, this method throws an error.
   * However, if the element is inside the `<label>` element that has an associated
   * [control](https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement/control), the control will be filled
   * instead.
   *
   * To send fine-grained keyboard events, use
   * [locator.type(text[, options])](https://playwright.dev/docs/api/class-locator#locator-type).
   */
  public async fill(locator: CustomLocator, value: string, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    timeout: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).fill(value, options);
  }

  /**
   * Calls [focus](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/focus) on the element.
   */
  public async focus(locator: CustomLocator, timeout: number = this.actionTimeout
  ): Promise<void> {
    await this.getLocator(locator).focus({ timeout: timeout });
  }

  /**
   * Focuses the element, and then uses [keyboard.down(key)](https://playwright.dev/docs/api/class-keyboard#keyboard-down)
   * and [keyboard.up(key)](https://playwright.dev/docs/api/class-keyboard#keyboard-up).
   *
   * `key` can specify the intended [keyboardEvent.key](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key)
   * value or a single character to generate the text for. A superset of the `key` values can be found
   * [here](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values). Examples of the keys are:
   *
   * `F1` - `F12`, `Digit0`- `Digit9`, `KeyA`- `KeyZ`, `Backquote`, `Minus`, `Equal`, `Backslash`, `Backspace`, `Tab`,
   * `Delete`, `Escape`, `ArrowDown`, `End`, `Enter`, `Home`, `Insert`, `PageDown`, `PageUp`, `ArrowRight`, `ArrowUp`, etc.
   *
   * Following modification shortcuts are also supported: `Shift`, `Control`, `Alt`, `Meta`, `ShiftLeft`.
   *
   * Holding down `Shift` will type the text that corresponds to the `key` in the upper case.
   *
   * If `key` is a single character, it is case-sensitive, so the values `a` and `A` will generate different respective
   * texts.
   *
   * Shortcuts such as `key: "Control+o"` or `key: "Control+Shift+T"` are supported as well. When specified with the
   * modifier, modifier is pressed and being held while the subsequent key is being pressed.
   * @param key Name of the key to press or a character to generate, such as `ArrowLeft` or `a`.
   * @param options
   */
  public async press(locator: CustomLocator, key: string, options: {
    delay?: number;
    noWaitAfter?: boolean;
    timeout?: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).press(key, options);
  }

  /**
    * This method waits for [actionability](https://playwright.dev/docs/actionability) checks, waits until all specified options are present in the
    * `<select>` element and selects these options.
    *
    * If the target element is not a `<select>` element, this method throws an error. However, if the element is inside the
    * `<label>` element that has an associated
    * [control](https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement/control), the control will be used instead.
    *
    * Returns the array of option values that have been successfully selected.
    *
    * Triggers a `change` and `input` event once all the provided options have been selected.
    *
    * ```js
    * // single selection matching the value
    * element.selectOption('blue');
    *
    * // single selection matching the label
    * element.selectOption({ label: 'Blue' });
    *
    * // multiple selection
    * element.selectOption(['red', 'green', 'blue']);
    * ```
    *
    * @param values Options to select. If the `<select>` has the `multiple` attribute, all matching options are selected, otherwise only the first option matching one of the passed options is selected. String values are equivalent to `{value:'string'}`. Option
    * is considered matching if all specified properties match.
    * @param options
    */
  public async selectOption(locator: CustomLocator, values: null | string | Array<string> | {
    value?: string;
    label?: string;
    index?: number;
  }, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    timeout: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).selectOption(values, options);
  }

  /**
   * This method captures a screenshot of the page, clipped to the size and position of a particular element matching the
   * locator. If the element is covered by other elements, it will not be actually visible on the screenshot. If the element
   * is a scrollable container, only the currently scrolled content will be visible on the screenshot.
   *
   * This method waits for the [actionability](https://playwright.dev/docs/actionability) checks, then scrolls element into view before taking a
   * screenshot. If the element is detached from DOM, the method throws an error.
   *
   * Returns the buffer with the captured screenshot.
   * @param options
   */
  public async takeElementScreenshot(locator: CustomLocator, options: LocatorScreenshotOptions = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).screenshot(options);
  }

  /**
   * This method returns the bounding box of the element, or `null` if the element is not visible. The bounding box is
   * calculated relative to the main frame viewport - which is usually the same as the browser window.
   *
   * Scrolling affects the returned bonding box, similarly to
   * [Element.getBoundingClientRect](https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect). That
   * means `x` and/or `y` may be negative.
   *
   * Elements from child frames return the bounding box relative to the main frame, unlike the
   * [Element.getBoundingClientRect](https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect).
   *
   * Assuming the page is static, it is safe to use bounding box coordinates to perform input. For example, the following
   * snippet should click the center of the element.
   *
   * ```js
   * const box = await element.boundingBox();
   * await page.mouse.click(box.x + box.width / 2, box.y + box.height / 2);
   * ```
   *
   * @param options
   */
  public async getBoundingBox(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<{
    x: number;
    y: number;
    width: number;
    height: number;
  }> {
    return (await this.getLocator(locator).boundingBox({ timeout: timeout }))!;
  }

  /**
   * Returns locator to the first matching element.
   */
  public getFirstElement(locator: CustomLocator): Locator {
    return this.getLocator(locator).first();
  }

  /**
   * Returns locator to the last matching element.
   */
  public getLastElement(locator: CustomLocator): Locator {
    return this.getLocator(locator).last();
  }

  /**
   * Returns locator to the n-th matching element. It's zero based, `nth(0)` selects the first element.
   * @param index
   */
  public getNthElement(locator: CustomLocator, index: number): Locator {
    return this.getLocator(locator).nth(index);
  }

  /**
   * This method narrows existing locator according to the options, for example filters by text.
   * @param options
   */
  public filter(locator: CustomLocator, options?: {
    has?: Locator;
    hasText?: string | RegExp;
  }): Locator {
    return this.getLocator(locator).filter(options);
  }


  /**
   * Custom drag and drop by bet bounding box
   */
  public async dragAndDropOnWeb(from: CustomLocator, destination: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    try {
      await this.waitFor(from, "attached", timeout)
      await this.waitFor(destination, "attached", timeout)

      let srcBound = await this.getLocator(from).boundingBox()
      let dstBound = await this.getLocator(destination).boundingBox();

      await this.page.mouse.move(srcBound.x + srcBound.width / 2, srcBound.y + srcBound.height / 2)
      await this.page.mouse.down();
      await this.page.mouse.move(dstBound.x + dstBound.width / 2, dstBound.y + dstBound.height / 2)
      await this.page.mouse.down();
    } catch (error) {
      throw new Error(error.message)
    }
  }

  /**
   * @destination  Locator of the element to drag to.
   */
  public async dragTo(from: CustomLocator, destination: CustomLocator, options: {
    force?: boolean;
    noWaitAfter?: boolean;
    sourcePosition?: {
      x: number;
      y: number;
    };
    targetPosition?: {
      x: number;
      y: number;
    };
    timeout?: number;
    trial?: boolean;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(from).dragTo(this.getLocator(destination), options)
  }

  /**
   * This method waits for [actionability](https://playwright.dev/docs/actionability) checks, then tries to scroll element into view, unless it is
   * completely visible as defined by
   * [IntersectionObserver](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API)'s `ratio`.
   */
  public async scrollIntoViewIfNeeded(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    await this.getFirstElement(locator).scrollIntoViewIfNeeded({ timeout: timeout });
  }

  /**
   * Scroll into view by use evaluate element.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' }), undefined, { timeout: timeout })
   */
  public async scrollIntoView(locator: CustomLocator, timeout: number = this.actionTimeout): Promise<void> {
    await this.getFirstElement(locator).evaluate(
      (ele) => ele.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' }), undefined, { timeout: timeout });
  }

  /**
   * Sets the value of the file input to these file paths or files. If some of the `filePaths` are relative paths, then they
   * are resolved relative to the current working directory. For empty array, clears the selected files.
   *
   * This method expects [Locator] to point to an
   * [input element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input). However, if the element is inside the
   * `<label>` element that has an associated
   * [control](https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement/control), targets the control instead.
   * @param files
   * @param options
   */
  public async setInputFiles(locator: CustomLocator, files: string | Array<string> | {
    name: string;
    mimeType: string;
    buffer: Buffer;
  } | Array<{
    name: string;
    mimeType: string;
    buffer: Buffer;
  }>, options: {
    noWaitAfter?: boolean;
    timeout: number;
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.getLocator(locator).setInputFiles(files, options);
  }


  // add custom methods here
  // If you need to access other helpers
  // use: this.helpers['helperName']

}

export = ElementWrapper;
