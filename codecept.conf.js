const config = require("./CommonWait.json")

const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');
// import config from "./common-config.json"

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

exports.config = {
  require: ['ts-node/register'],
  tests: './*_test.ts',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost',
      show: true,
      browser: 'chromium',
      timeout: config.timeout,
      waitForAction: config.actionTimeout,
      waitForTimeout: config.timeout,
      waitForNavigation: "load",
      windowSize: "1280x720",
      video: true,
      trace: true,
    },
    playwright_extent: {
      require: './PlaywrightExtentHelper',
    },
    ChaiWrapper: {
      "require": "codeceptjs-chai"
    }
  },
  // include: {
  //   I: './steps_file.js'
  // },
  bootstrap: null,
  mocha: {},
  name: 'codeceptjsDraft'
}
