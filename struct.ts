import { APIResponse, ElementHandle, JSHandle, Locator, Page } from "playwright";
import type * as expectType from '@playwright/test/types/expect-types';

export interface PageWaitForFunctionOptions {
    /**
     * If `polling` is `'raf'`, then `pageFunction` is constantly executed in `requestAnimationFrame` callback. If `polling` is
     * a number, then it is treated as an interval in milliseconds at which the function would be executed. Defaults to `raf`.
     */
    polling?: number | "raf";

    /**
     * maximum time to wait for in milliseconds. Defaults to `30000` (30 seconds). Pass `0` to disable timeout. The default
     * value can be changed by using the
     * [browserContext.setDefaultTimeout(timeout)](https://playwright.dev/docs/api/class-browsercontext#browser-context-set-default-timeout).
     */
    timeout?: number;
}
export interface CustomLocator {
    frameSelector?: string[]
    selector: string | CodeceptJS.LocatorOrString,
}
export interface PageWaitForSelectorOptions {
    /**
     * Defaults to `'visible'`. Can be either:
     * - `'attached'` - wait for element to be present in DOM.
     * - `'detached'` - wait for element to not be present in DOM.
     * - `'visible'` - wait for element to have non-empty bounding box and no `visibility:hidden`. Note that element without
     *   any content or with `display:none` has an empty bounding box and is not considered visible.
     * - `'hidden'` - wait for element to be either detached from DOM, or have an empty bounding box or `visibility:hidden`.
     *   This is opposite to the `'visible'` option.
     */
    state?: "attached" | "detached" | "visible" | "hidden";

    /**
     * When true, the call requires selector to resolve to a single element. If given selector resolves to more then one
     * element, the call throws an exception.
     */
    strict?: boolean;

    /**
     * Maximum time in milliseconds, defaults to 30 seconds, pass `0` to disable timeout. The default value can be changed by
     * using the
     * [browserContext.setDefaultTimeout(timeout)](https://playwright.dev/docs/api/class-browsercontext#browser-context-set-default-timeout)
     * or [page.setDefaultTimeout(timeout)](https://playwright.dev/docs/api/class-page#page-set-default-timeout) methods.
     */
    timeout?: number;
}
export interface ScreenshotAssertions {
    /**
     * Ensures that passed value, either a [string] or a [Buffer], matches the expected snapshot stored in the test snapshots
     * directory.
     *
     * ```js
     * // Basic usage.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png');
     *
     * // Pass options to customize the snapshot comparison and have a generated name.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png', {
     *   maxDiffPixels: 27, // allow no more than 27 different pixels.
     * });
     *
     * // Configure image matching threshold.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png', { threshold: 0.3 });
     *
     * // Bring some structure to your snapshot files by passing file path segments.
     * expect(await page.screenshot()).toMatchSnapshot(['landing', 'step2.png']);
     * expect(await page.screenshot()).toMatchSnapshot(['landing', 'step3.png']);
     * ```
     *
     * Learn more about [visual comparisons](https://playwright.dev/docs/api/test-snapshots).
     * @param name Snapshot name.
     * @param options
     */
    toMatchSnapshot(name: string | Array<string>, options?: {
        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;
    }): void;

    /**
     * Ensures that passed value, either a [string] or a [Buffer], matches the expected snapshot stored in the test snapshots
     * directory.
     *
     * ```js
     * // Basic usage and the file name is derived from the test name.
     * expect(await page.screenshot()).toMatchSnapshot();
     *
     * // Pass options to customize the snapshot comparison and have a generated name.
     * expect(await page.screenshot()).toMatchSnapshot({
     *   maxDiffPixels: 27, // allow no more than 27 different pixels.
     * });
     *
     * // Configure image matching threshold and snapshot name.
     * expect(await page.screenshot()).toMatchSnapshot({
     *   name: 'landing-page.png',
     *   threshold: 0.3,
     * });
     * ```
     *
     * Learn more about [visual comparisons](https://playwright.dev/docs/api/test-snapshots).
     * @param options
     */
    toMatchSnapshot(options?: {
        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Snapshot name. If not passed, the test name and ordinals are used when called multiple times.
         */
        name?: string | Array<string>;

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;
    }): void;
}

export interface PageAssertions {
    /**
     * Makes the assertion check for the opposite condition. For example, this code tests that the page URL doesn't contain
     * `"error"`:
     *
     * ```js
     * await expect(page).not.toHaveURL('error');
     * ```
     *
     */
    not: PageAssertions;

    /**
     * Ensures that the page resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * await expect(page).toHaveScreenshot('image.png');
     * ```
     *
     * @param name Snapshot name.
     * @param options
     */
    toHaveScreenshot(name: string | Array<string>, options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * An object which specifies clipping of the resulting image. Should have the following fields:
         */
        clip?: {
            /**
             * x-coordinate of top-left corner of clip area
             */
            x: number;

            /**
             * y-coordinate of top-left corner of clip area
             */
            y: number;

            /**
             * width of clipping area
             */
            width: number;

            /**
             * height of clipping area
             */
            height: number;
        };

        /**
         * When true, takes a screenshot of the full scrollable page, instead of the currently visible viewport. Defaults to
         * `false`.
         */
        fullPage?: boolean;

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that the page resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * await expect(page).toHaveScreenshot();
     * ```
     *
     * @param options
     */
    toHaveScreenshot(options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * An object which specifies clipping of the resulting image. Should have the following fields:
         */
        clip?: {
            /**
             * x-coordinate of top-left corner of clip area
             */
            x: number;

            /**
             * y-coordinate of top-left corner of clip area
             */
            y: number;

            /**
             * width of clipping area
             */
            width: number;

            /**
             * height of clipping area
             */
            height: number;
        };

        /**
         * When true, takes a screenshot of the full scrollable page, instead of the currently visible viewport. Defaults to
         * `false`.
         */
        fullPage?: boolean;

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the page has the given title.
     *
     * ```js
     * await expect(page).toHaveTitle(/.*checkout/);
     * ```
     *
     * @param titleOrRegExp Expected title or RegExp.
     * @param options
     */
    toHaveTitle(titleOrRegExp: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the page is navigated to the given URL.
     *
     * ```js
     * await expect(page).toHaveURL(/.*checkout/);
     * ```
     *
     * @param urlOrRegExp Expected substring or RegExp.
     * @param options
     */
    toHaveURL(urlOrRegExp: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;
}

export interface LocatorAssertions {
    /**
     * Makes the assertion check for the opposite condition. For example, this code tests that the Locator doesn't contain text
     * `"error"`:
     *
     * ```js
     * await expect(locator).not.toContainText('error');
     * ```
     *
     */
    not: LocatorAssertions;

    /**
     * Ensures the [Locator] points to a checked input.
     *
     * ```js
     * const locator = page.locator('.subscribe');
     * await expect(locator).toBeChecked();
     * ```
     *
     * @param options
     */
    toBeChecked(options?: {
        checked?: boolean;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a disabled element. Element is disabled if it has "disabled" attribute or is disabled
     * via ['aria-disabled'](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-disabled). Note
     * that only native control elements such as HTML `button`, `input`, `select`, `textarea`, `option`, `optgroup` can be
     * disabled by setting "disabled" attribute. "disabled" attribute on other elements is ignored by the browser.
     *
     * ```js
     * const locator = page.locator('button.submit');
     * await expect(locator).toBeDisabled();
     * ```
     *
     * @param options
     */
    toBeDisabled(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an editable element.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toBeEditable();
     * ```
     *
     * @param options
     */
    toBeEditable(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an empty editable element or to a DOM node that has no text.
     *
     * ```js
     * const locator = page.locator('div.warning');
     * await expect(locator).toBeEmpty();
     * ```
     *
     * @param options
     */
    toBeEmpty(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an enabled element.
     *
     * ```js
     * const locator = page.locator('button.submit');
     * await expect(locator).toBeEnabled();
     * ```
     *
     * @param options
     */
    toBeEnabled(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a focused DOM node.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toBeFocused();
     * ```
     *
     * @param options
     */
    toBeFocused(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a hidden DOM node, which is the opposite of [visible](https://playwright.dev/docs/api/actionability#visible).
     *
     * ```js
     * const locator = page.locator('.my-element');
     * await expect(locator).toBeHidden();
     * ```
     *
     * @param options
     */
    toBeHidden(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a [visible](https://playwright.dev/docs/api/actionability#visible) DOM node.
     *
     * ```js
     * const locator = page.locator('.my-element');
     * await expect(locator).toBeVisible();
     * ```
     *
     * @param options
     */
    toBeVisible(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element that contains the given text. You can use regular expressions for the value
     * as well.
     *
     * ```js
     * const locator = page.locator('.title');
     * await expect(locator).toContainText('substring');
     * await expect(locator).toContainText(/\d messages/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .list-item');
     * await expect(locator).toContainText(['Text 1', 'Text 4', 'Text 5']);
     * ```
     *
     * @param expected Expected substring or RegExp or a list of those.
     * @param options
     */
    toContainText(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;

        /**
         * Whether to use `element.innerText` instead of `element.textContent` when retrieving DOM node text.
         */
        useInnerText?: boolean;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given attribute.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toHaveAttribute('type', 'text');
     * ```
     *
     * @param name Attribute name.
     * @param value Expected attribute value.
     * @param options
     */
    toHaveAttribute(name: string, value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given CSS class.
     *
     * ```js
     * const locator = page.locator('#component');
     * await expect(locator).toHaveClass(/selected/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .component');
     * await expect(locator).toHaveClass(['component', 'component selected', 'component']);
     * ```
     *
     * @param expected Expected class or RegExp or a list of those.
     * @param options
     */
    toHaveClass(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] resolves to an exact number of DOM nodes.
     *
     * ```js
     * const list = page.locator('list > .component');
     * await expect(list).toHaveCount(3);
     * ```
     *
     * @param count Expected count.
     * @param options
     */
    toHaveCount(count: number, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] resolves to an element with the given computed CSS style.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveCSS('display', 'flex');
     * ```
     *
     * @param name CSS property name.
     * @param value CSS property value.
     * @param options
     */
    toHaveCSS(name: string, value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given DOM Node ID.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toHaveId('lastname');
     * ```
     *
     * @param id Element id.
     * @param options
     */
    toHaveId(id: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given JavaScript property. Note that this property can be of a primitive
     * type as well as a plain serializable JavaScript object.
     *
     * ```js
     * const locator = page.locator('.component');
     * await expect(locator).toHaveJSProperty('loaded', true);
     * ```
     *
     * @param name Property name.
     * @param value Property value.
     * @param options
     */
    toHaveJSProperty(name: string, value: any, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that [Locator] resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveScreenshot('image.png');
     * ```
     *
     * @param name Snapshot name.
     * @param options
     */
    toHaveScreenshot(name: string | Array<string>, options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that [Locator] resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveScreenshot();
     * ```
     *
     * @param options
     */
    toHaveScreenshot(options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given text. You can use regular expressions for the value as well.
     *
     * ```js
     * const locator = page.locator('.title');
     * await expect(locator).toHaveText(/Welcome, Test User/);
     * await expect(locator).toHaveText(/Welcome, .*\/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .component');
     * await expect(locator).toHaveText(['Text 1', 'Text 2', 'Text 3']);
     * ```
     *
     * @param expected Expected substring or RegExp or a list of those.
     * @param options
     */
    toHaveText(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;

        /**
         * Whether to use `element.innerText` instead of `element.textContent` when retrieving DOM node text.
         */
        useInnerText?: boolean;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given input value. You can use regular expressions for the value as
     * well.
     *
     * ```js
     * const locator = page.locator('input[type=number]');
     * await expect(locator).toHaveValue(/[0-9]/);
     * ```
     *
     * @param value Expected value.
     * @param options
     */
    toHaveValue(value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;
}

export interface APIResponseAssertions {
    /**
     * Makes the assertion check for the opposite condition. For example, this code tests that the response status is not
     * successful:
     *
     * ```js
     * await expect(response).not.toBeOK();
     * ```
     *
     */
    not: APIResponseAssertions;

    /**
     * Ensures the response status code is within [200..299] range.
     *
     * ```js
     * await expect(response).toBeOK();
     * ```
     *
     */
    toBeOK(): Promise<void>;
}

export interface LocatorAssertions {
    /**
     * Makes the assertion check for the opposite condition. For example, this code tests that the Locator doesn't contain text
     * `"error"`:
     *
     * ```js
     * await expect(locator).not.toContainText('error');
     * ```
     *
     */
    not: LocatorAssertions;

    /**
     * Ensures the [Locator] points to a checked input.
     *
     * ```js
     * const locator = page.locator('.subscribe');
     * await expect(locator).toBeChecked();
     * ```
     *
     * @param options
     */
    toBeChecked(options?: {
        checked?: boolean;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a disabled element. Element is disabled if it has "disabled" attribute or is disabled
     * via ['aria-disabled'](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-disabled). Note
     * that only native control elements such as HTML `button`, `input`, `select`, `textarea`, `option`, `optgroup` can be
     * disabled by setting "disabled" attribute. "disabled" attribute on other elements is ignored by the browser.
     *
     * ```js
     * const locator = page.locator('button.submit');
     * await expect(locator).toBeDisabled();
     * ```
     *
     * @param options
     */
    toBeDisabled(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an editable element.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toBeEditable();
     * ```
     *
     * @param options
     */
    toBeEditable(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an empty editable element or to a DOM node that has no text.
     *
     * ```js
     * const locator = page.locator('div.warning');
     * await expect(locator).toBeEmpty();
     * ```
     *
     * @param options
     */
    toBeEmpty(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an enabled element.
     *
     * ```js
     * const locator = page.locator('button.submit');
     * await expect(locator).toBeEnabled();
     * ```
     *
     * @param options
     */
    toBeEnabled(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a focused DOM node.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toBeFocused();
     * ```
     *
     * @param options
     */
    toBeFocused(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a hidden DOM node, which is the opposite of [visible](https://playwright.dev/docs/api/actionability#visible).
     *
     * ```js
     * const locator = page.locator('.my-element');
     * await expect(locator).toBeHidden();
     * ```
     *
     * @param options
     */
    toBeHidden(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to a [visible](https://playwright.dev/docs/api/actionability#visible) DOM node.
     *
     * ```js
     * const locator = page.locator('.my-element');
     * await expect(locator).toBeVisible();
     * ```
     *
     * @param options
     */
    toBeVisible(options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element that contains the given text. You can use regular expressions for the value
     * as well.
     *
     * ```js
     * const locator = page.locator('.title');
     * await expect(locator).toContainText('substring');
     * await expect(locator).toContainText(/\d messages/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .list-item');
     * await expect(locator).toContainText(['Text 1', 'Text 4', 'Text 5']);
     * ```
     *
     * @param expected Expected substring or RegExp or a list of those.
     * @param options
     */
    toContainText(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;

        /**
         * Whether to use `element.innerText` instead of `element.textContent` when retrieving DOM node text.
         */
        useInnerText?: boolean;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given attribute.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toHaveAttribute('type', 'text');
     * ```
     *
     * @param name Attribute name.
     * @param value Expected attribute value.
     * @param options
     */
    toHaveAttribute(name: string, value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given CSS class.
     *
     * ```js
     * const locator = page.locator('#component');
     * await expect(locator).toHaveClass(/selected/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .component');
     * await expect(locator).toHaveClass(['component', 'component selected', 'component']);
     * ```
     *
     * @param expected Expected class or RegExp or a list of those.
     * @param options
     */
    toHaveClass(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] resolves to an exact number of DOM nodes.
     *
     * ```js
     * const list = page.locator('list > .component');
     * await expect(list).toHaveCount(3);
     * ```
     *
     * @param count Expected count.
     * @param options
     */
    toHaveCount(count: number, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] resolves to an element with the given computed CSS style.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveCSS('display', 'flex');
     * ```
     *
     * @param name CSS property name.
     * @param value CSS property value.
     * @param options
     */
    toHaveCSS(name: string, value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given DOM Node ID.
     *
     * ```js
     * const locator = page.locator('input');
     * await expect(locator).toHaveId('lastname');
     * ```
     *
     * @param id Element id.
     * @param options
     */
    toHaveId(id: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with given JavaScript property. Note that this property can be of a primitive
     * type as well as a plain serializable JavaScript object.
     *
     * ```js
     * const locator = page.locator('.component');
     * await expect(locator).toHaveJSProperty('loaded', true);
     * ```
     *
     * @param name Property name.
     * @param value Property value.
     * @param options
     */
    toHaveJSProperty(name: string, value: any, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that [Locator] resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveScreenshot('image.png');
     * ```
     *
     * @param name Snapshot name.
     * @param options
     */
    toHaveScreenshot(name: string | Array<string>, options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that [Locator] resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * const locator = page.locator('button');
     * await expect(locator).toHaveScreenshot();
     * ```
     *
     * @param options
     */
    toHaveScreenshot(options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given text. You can use regular expressions for the value as well.
     *
     * ```js
     * const locator = page.locator('.title');
     * await expect(locator).toHaveText(/Welcome, Test User/);
     * await expect(locator).toHaveText(/Welcome, .*\/);
     * ```
     *
     * Note that if array is passed as an expected value, entire lists of elements can be asserted:
     *
     * ```js
     * const locator = page.locator('list > .component');
     * await expect(locator).toHaveText(['Text 1', 'Text 2', 'Text 3']);
     * ```
     *
     * @param expected Expected substring or RegExp or a list of those.
     * @param options
     */
    toHaveText(expected: string | RegExp | Array<string | RegExp>, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;

        /**
         * Whether to use `element.innerText` instead of `element.textContent` when retrieving DOM node text.
         */
        useInnerText?: boolean;
    }): Promise<void>;

    /**
     * Ensures the [Locator] points to an element with the given input value. You can use regular expressions for the value as
     * well.
     *
     * ```js
     * const locator = page.locator('input[type=number]');
     * await expect(locator).toHaveValue(/[0-9]/);
     * ```
     *
     * @param value Expected value.
     * @param options
     */
    toHaveValue(value: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;
}

export interface PageAssertions {
    /**
     * Makes the assertion check for the opposite condition. For example, this code tests that the page URL doesn't contain
     * `"error"`:
     *
     * ```js
     * await expect(page).not.toHaveURL('error');
     * ```
     *
     */
    not: PageAssertions;

    /**
     * Ensures that the page resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * await expect(page).toHaveScreenshot('image.png');
     * ```
     *
     * @param name Snapshot name.
     * @param options
     */
    toHaveScreenshot(name: string | Array<string>, options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * An object which specifies clipping of the resulting image. Should have the following fields:
         */
        clip?: {
            /**
             * x-coordinate of top-left corner of clip area
             */
            x: number;

            /**
             * y-coordinate of top-left corner of clip area
             */
            y: number;

            /**
             * width of clipping area
             */
            width: number;

            /**
             * height of clipping area
             */
            height: number;
        };

        /**
         * When true, takes a screenshot of the full scrollable page, instead of the currently visible viewport. Defaults to
         * `false`.
         */
        fullPage?: boolean;

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures that the page resolves to a given screenshot. This function will re-take screenshots until it matches with the
     * saved expectation.
     *
     * If there's no expectation yet, it will wait until two consecutive screenshots yield the same result, and save the last
     * one as an expectation.
     *
     * ```js
     * await expect(page).toHaveScreenshot();
     * ```
     *
     * @param options
     */
    toHaveScreenshot(options?: {
        /**
         * When set to `"disabled"`, stops CSS animations, CSS transitions and Web Animations. Animations get different treatment
         * depending on their duration:
         * - finite animations are fast-forwarded to completion, so they'll fire `transitionend` event.
         * - infinite animations are canceled to initial state, and then played over after the screenshot.
         *
         * Defaults to `"disabled"` that disables animations.
         */
        animations?: "disabled" | "allow";

        /**
         * When set to `"hide"`, screenshot will hide text caret. When set to `"initial"`, text caret behavior will not be changed.
         * Defaults to `"hide"`.
         */
        caret?: "hide" | "initial";

        /**
         * An object which specifies clipping of the resulting image. Should have the following fields:
         */
        clip?: {
            /**
             * x-coordinate of top-left corner of clip area
             */
            x: number;

            /**
             * y-coordinate of top-left corner of clip area
             */
            y: number;

            /**
             * width of clipping area
             */
            width: number;

            /**
             * height of clipping area
             */
            height: number;
        };

        /**
         * When true, takes a screenshot of the full scrollable page, instead of the currently visible viewport. Defaults to
         * `false`.
         */
        fullPage?: boolean;

        /**
         * Specify locators that should be masked when the screenshot is taken. Masked elements will be overlayed with a pink box
         * `#FF00FF` that completely covers its bounding box.
         */
        mask?: Array<Locator>;

        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Hides default white background and allows capturing screenshots with transparency. Not applicable to `jpeg` images.
         * Defaults to `false`.
         */
        omitBackground?: boolean;

        /**
         * When set to `"css"`, screenshot will have a single pixel per each css pixel on the page. For high-dpi devices, this will
         * keep screenshots small. Using `"device"` option will produce a single pixel per each device pixel, so screenhots of
         * high-dpi devices will be twice as large or even larger.
         *
         * Defaults to `"css"`.
         */
        scale?: "css" | "device";

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;

        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the page has the given title.
     *
     * ```js
     * await expect(page).toHaveTitle(/.*checkout/);
     * ```
     *
     * @param titleOrRegExp Expected title or RegExp.
     * @param options
     */
    toHaveTitle(titleOrRegExp: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;

    /**
     * Ensures the page is navigated to the given URL.
     *
     * ```js
     * await expect(page).toHaveURL(/.*checkout/);
     * ```
     *
     * @param urlOrRegExp Expected substring or RegExp.
     * @param options
     */
    toHaveURL(urlOrRegExp: string | RegExp, options?: {
        /**
         * Time to retry the assertion for. Defaults to `timeout` in `TestConfig.expect`.
         */
        timeout?: number;
    }): Promise<void>;
}

export interface ScreenshotAssertions {
    /**
     * Ensures that passed value, either a [string] or a [Buffer], matches the expected snapshot stored in the test snapshots
     * directory.
     *
     * ```js
     * // Basic usage.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png');
     *
     * // Pass options to customize the snapshot comparison and have a generated name.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png', {
     *   maxDiffPixels: 27, // allow no more than 27 different pixels.
     * });
     *
     * // Configure image matching threshold.
     * expect(await page.screenshot()).toMatchSnapshot('landing-page.png', { threshold: 0.3 });
     *
     * // Bring some structure to your snapshot files by passing file path segments.
     * expect(await page.screenshot()).toMatchSnapshot(['landing', 'step2.png']);
     * expect(await page.screenshot()).toMatchSnapshot(['landing', 'step3.png']);
     * ```
     *
     * Learn more about [visual comparisons](https://playwright.dev/docs/api/test-snapshots).
     * @param name Snapshot name.
     * @param options
     */
    toMatchSnapshot(name: string | Array<string>, options?: {
        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;
    }): void;

    /**
     * Ensures that passed value, either a [string] or a [Buffer], matches the expected snapshot stored in the test snapshots
     * directory.
     *
     * ```js
     * // Basic usage and the file name is derived from the test name.
     * expect(await page.screenshot()).toMatchSnapshot();
     *
     * // Pass options to customize the snapshot comparison and have a generated name.
     * expect(await page.screenshot()).toMatchSnapshot({
     *   maxDiffPixels: 27, // allow no more than 27 different pixels.
     * });
     *
     * // Configure image matching threshold and snapshot name.
     * expect(await page.screenshot()).toMatchSnapshot({
     *   name: 'landing-page.png',
     *   threshold: 0.3,
     * });
     * ```
     *
     * Learn more about [visual comparisons](https://playwright.dev/docs/api/test-snapshots).
     * @param options
     */
    toMatchSnapshot(options?: {
        /**
         * An acceptable ratio of pixels that are different to the total amount of pixels, between `0` and `1`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixelRatio?: number;

        /**
         * An acceptable amount of pixels that could be different, default is configurable with `TestConfig.expect`. Default is
         * configurable with `TestConfig.expect`. Unset by default.
         */
        maxDiffPixels?: number;

        /**
         * Snapshot name. If not passed, the test name and ordinals are used when called multiple times.
         */
        name?: string | Array<string>;

        /**
         * An acceptable perceived color difference in the [YIQ color space](https://en.wikipedia.org/wiki/YIQ) between the same
         * pixel in compared images, between zero (strict) and one (lax), default is configurable with `TestConfig.expect`.
         * Defaults to `0.2`.
         */
        threshold?: number;
    }): void;
}

export interface TestError {
    /**
     * Error message. Set when [Error] (or its subclass) has been thrown.
     */
    message?: string;

    /**
     * Error stack. Set when [Error] (or its subclass) has been thrown.
     */
    stack?: string;

    /**
     * The value that was thrown. Set when anything except the [Error] (or its subclass) has been thrown.
     */
    value?: string;
}

export type NoHandles<Arg> = Arg extends JSHandle ? never : (Arg extends object ? { [Key in keyof Arg]: NoHandles<Arg[Key]> } : Arg);
export type Unboxed<Arg> =
    Arg extends ElementHandle<infer T> ? T :
    Arg extends JSHandle<infer T> ? T :
    Arg extends NoHandles<Arg> ? Arg :
    Arg extends [infer A0] ? [Unboxed<A0>] :
    Arg extends [infer A0, infer A1] ? [Unboxed<A0>, Unboxed<A1>] :
    Arg extends [infer A0, infer A1, infer A2] ? [Unboxed<A0>, Unboxed<A1>, Unboxed<A2>] :
    Arg extends [infer A0, infer A1, infer A2, infer A3] ? [Unboxed<A0>, Unboxed<A1>, Unboxed<A2>, Unboxed<A3>] :
    Arg extends Array<infer T> ? Array<Unboxed<T>> :
    Arg extends object ? { [Key in keyof Arg]: Unboxed<Arg[Key]> } :
    Arg;
export type SmartHandle<T> = T extends Node ? ElementHandle<T> : JSHandle<T>;
export type PageFunction<Arg, R> = string | ((arg: Unboxed<Arg>) => R | Promise<R>);

export type PageWaitForSelectorOptionsNotHidden = PageWaitForSelectorOptions & {
    state?: 'visible' | 'attached';
};

export type ElementHandleForTag<K extends keyof HTMLElementTagNameMap> = ElementHandle<HTMLElementTagNameMap[K]>;
export type Expect = {
    <T = unknown>(actual: T, messageOrOptions?: string | { message?: string }): MakeMatchers<void, T>;
    soft: <T = unknown>(actual: T, messageOrOptions?: string | { message?: string }) => MakeMatchers<void, T>;
    poll: <T = unknown>(actual: () => T | Promise<T>, messageOrOptions?: string | { message?: string, timeout?: number, intervals?: number[] }) => BaseMatchers<Promise<void>, T> & {
        /**
         * If you know how to test something, `.not` lets you test its opposite.
         */
        not: BaseMatchers<Promise<void>, T>;
    };
}
export type IfAny<T, Y, N> = 0 extends (1 & T) ? Y : N;
export type ExtraMatchers<T, Type, Matchers> = T extends Type ? Matchers : IfAny<T, Matchers, {}>;
export type BaseMatchers<R, T> = expectType.Matchers<R> & PlaywrightTest.Matchers<R, T>;
export type MakeMatchers<R, T> = BaseMatchers<R, T> & {
    /**
     * If you know how to test something, `.not` lets you test its opposite.
     */
    not: MakeMatchers<R, T>;
    /**
     * Use resolves to unwrap the value of a fulfilled promise so any other
     * matcher can be chained. If the promise is rejected the assertion fails.
     */
    resolves: MakeMatchers<Promise<R>, Awaited<T>>;
    /**
    * Unwraps the reason of a rejected promise so any other matcher can be chained.
    * If the promise is fulfilled the assertion fails.
    */
    rejects: MakeMatchers<Promise<R>, Awaited<T>>;
} & ScreenshotAssertions &
    ExtraMatchers<T, Page, PageAssertions> &
    ExtraMatchers<T, Locator, LocatorAssertions> &
    ExtraMatchers<T, APIResponse, APIResponseAssertions>;

