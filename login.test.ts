import { CustomLocator } from './struct';

Feature('login');

Scenario('test something', async ({ I }) => {
    let newContext = await I.createNewContext()
    await newContext.newPage()

    await I.waitForLoadPage
    await I.goto("https://www.google.com/")
    console.log(await I.getTitle());

    await I.setContext(2);
    await I.bringToFront(1)

    await I.goto("https://www.mozilla.org/en-US/firefox/new/")
    console.log(await I.getTitle());
    await newContext.close()
});

Scenario('test get title', async ({ I }) => {
    let location: string = "Hồ Chí Minh";
    let chuyenkhoa = "Chuyên khoa";
    let dichvu = "Dịch vụ"
    let chuyenkhoaLocator: CustomLocator = { selector: `(//header[div[p[normalize-space(.)='${chuyenkhoa}']]]/following-sibling::section/ul)[1]//li//p` };
    let dichvuLocator: CustomLocator = { selector: `(//header[div[p[normalize-space(.)='${dichvu}']]]/following-sibling::section/ul)[1]//li//p` };

    await I.say("aa")
    await I.goto("https://hellobacsi.com/care/")
    await I.scrollIntoView({ selector: "#search-location,input[value='Vị trí hiện tại']" })
    await I.clickOnWeb({ selector: "#search-location,input[value='Vị trí hiện tại']" })
    await I.clickOnWeb({ selector: `div[data-testid='location-search-results'] p:text('${location}')` })

    // await console.log(new Date().getSeconds());
    // await I.waitForAllElementVisibleOnWeb(chuyenkhoaLocator, 5000)
    // await console.log(new Date().getSeconds());

    let listChuyenkhoa = await I.getAllText(chuyenkhoaLocator, "textPresent");
    console.log(listChuyenkhoa);

    // await console.log(new Date().getSeconds());
    // await I.waitUntilAllElementHiddenOnWeb({ selector: "xxx" }, 5000)
    // await console.log(new Date().getSeconds());

    let listDichVu = await I.getAllText(dichvuLocator);
    console.log(listDichVu);
});

Scenario('frame test', async ({ I }) => {
    await I.goto("https://letcode.in/frame")
    await I.fill({ frameSelector: ["#firstFr"], selector: "input[name='fname']" }, "Koushik")
    await I.fill({ frameSelector: ["#firstFr"], selector: "input[name='lname']" }, "Chatterjee")
    await I.fill({ frameSelector: ["#firstFr", "iframe[src='innerFrame']"], selector: "input[name='email']" }, "koushik@mail.com")
    await I.fill({ frameSelector: ["#firstFr"], selector: "input[name='lname']" }, "Youtube")
});


Scenario('drag test', async ({ I }) => {
    await I.goto("https://jqueryui.com/droppable/");
    await I.dragTo(
        { frameSelector: ["iframe[class='demo-frame']"], selector: "#draggable" },
        { frameSelector: ["iframe[class='demo-frame']"], selector: "#droppable" });
    await I.waitForTimeout(2)

    await I.goto("https://david-desmaisons.github.io/draggable-example/");
    await I.dragTo({ selector: "span li:nth-child(1)" }, { selector: "span li:nth-child(2)" })
    await I.waitForTimeout(2)

});
