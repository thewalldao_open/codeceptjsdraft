import Helper from '@codeceptjs/helper';
import { Page, Browser, BrowserContext, PageScreenshotOptions, BrowserContextOptions, ElementHandle, ConsoleMessage, Dialog, Download, FileChooser, Frame, Response, Request } from "playwright";
import { ElementHandleForTag, PageFunction, PageWaitForFunctionOptions, PageWaitForSelectorOptions, PageWaitForSelectorOptionsNotHidden, SmartHandle } from './struct';
import * as configAll from "./CommonWait.json"


class BrowserWrapper extends Helper {
  protected page: Page;
  protected browser: Browser;
  protected context: BrowserContext;
  protected actionTimeout: number = configAll.actionTimeout * 1000;
  protected navigateTimeout: number = configAll.navigationTimeout * 1000;
  protected shortTimeout: number = configAll.actionShortTimeout * 1000;
  protected isTimeout: number = configAll.isTimeout * 1000;
  protected baseUrl: string = ""


  constructor(config: any) {
    super(config);
    this.baseUrl = config.url;
    this.helpers
  }

  /**
   * @protected
   */
  public async _before() {
    this.browser = await this.helpers.Playwright.browser
    this.context = await this.helpers.Playwright.browserContext;
    this.page = await this.helpers.Playwright.page;
  }

  /**
   * @usage wait for page load to state (document.readyState == 'complete)
   * @param timeout 
   */
  public async waitForLoadPage(timeout: number = configAll.navigationTimeout) {
    await this.waitForFunction(() => document.readyState == 'complete', undefined, { timeout: timeout })
  }

  /**
   * Set context to interact with the context and It's page
   */
  public async setContext(context: number | BrowserContext) {
    if (typeof (context) == "number") {
      let allContexts: BrowserContext[] = await this.getContexts();
      this.context = allContexts[context - 1]
    } else {
      this.context = context;
    }
    return this;
  }

  /**
   * 
   * Set page to interact with it
   */
  public async setPage(page: Page) {
    this.page = page;
  }

  /**
   * 
   * Get current context
   */
  public async getContext(): Promise<BrowserContext> {
    return this.context;
  }

  /**
   * 
   * Get current browser
   */
  public async getBrowser(): Promise<Browser> {
    return this.browser;
  }

  /**
   * 
   * Get current page
   */
  public async getPage(): Promise<Page> {
    return this.page
  }

  /**
     * In the case of multiple pages in a single browser, each page can have its own viewport size. However,
     * [browser.newContext([options])](https://playwright.dev/docs/api/class-browser#browser-new-context) allows to set
     * viewport size (and more) for all pages in the context at once.
     *
     * [page.setViewportSize(viewportSize)](https://playwright.dev/docs/api/class-page#page-set-viewport-size) will resize the
     * page. A lot of websites don't expect phones to change size, so you should set the viewport size before navigating to the
     * page. [page.setViewportSize(viewportSize)](https://playwright.dev/docs/api/class-page#page-set-viewport-size) will also
     * reset `screen` size, use
     * [browser.newContext([options])](https://playwright.dev/docs/api/class-browser#browser-new-context) with `screen` and
     * `viewport` parameters if you need better control of these properties.
     *
     * @param viewportSize
     */
  public async setViewPortSize(viewportSize: {
    width: number;
    height: number;
  }): Promise<void> {
    return await this.page.setViewportSize(viewportSize)
  }

  /**
   * Returns the buffer with the captured screenshot.
   * @param options
   */
  public async screenshot(options?: PageScreenshotOptions): Promise<Buffer> {
    return await this.page.screenshot(options)
  }

  /**
  * Creates a new page in the browser context.
  */
  public async createNewPage(): Promise<Page> {
    return await this.context.newPage()
  }

  /**
   * Creates a new browser context. It won't share cookies/cache with other browser contexts.
   *
   * @param options
   */
  public async createNewContext(options?: BrowserContextOptions): Promise<BrowserContext> {
    return await this.browser.newContext(options)
  }

  /**
   * Returns the page's title. Shortcut for main frame's
   * [frame.title()](https://playwright.dev/docs/api/class-frame#frame-title).
   */
  public async getTitle(): Promise<string> {
    return await this.page.title()
  }

  /**
   * Returns the main resource response. In case of multiple redirects, the navigation will resolve with the first
   * non-redirect response.
   *
   * The method will throw an error if:
   * - there's an SSL error (e.g. in case of self-signed certificates).
   * - target URL is invalid.
   * - the `timeout` is exceeded during navigation.
   * - the remote server does not respond or is unreachable.
   * - the main resource failed to load.
   *
   * The method will not throw an error when any valid HTTP status code is returned by the remote server, including 404 "Not
   * Found" and 500 "Internal Server Error".  The status code for such responses can be retrieved by calling
   * [response.status()](https://playwright.dev/docs/api/class-response#response-status).
   *
   * > NOTE: The method either throws an error or returns a main resource response. The only exceptions are navigation to
   * `about:blank` or navigation to the same URL with a different hash, which would succeed and return `null`.
   * > NOTE: Headless mode doesn't support navigation to a PDF document. See the
   * [upstream issue](https://bugs.chromium.org/p/chromium/issues/detail?id=761295).
   *
   * Shortcut for main frame's [frame.goto(url[, options])](https://playwright.dev/docs/api/class-frame#frame-goto)
   * @param url URL to navigate page to. The url should include scheme, e.g. `https://`. When a `baseURL` via the context options was provided and the passed URL is a path, it gets merged via the
   * [`new URL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL/URL) constructor.
   * @param options
   */
  async goto(url: string, opt?: {
    referer?: string;
    timeout?: number;
    waitUntil?: "load" | "domcontentloaded" | "networkidle" | "commit";
  }) {
    if (this.baseUrl) {
      await this.page.goto(this.baseUrl + url, opt);
    } else {
      await this.page.goto(url, opt);
    }
    await this.waitForLoadPage()
  }

  /**
   * 
   * Returns Page's url
   */
  public async getUrl(): Promise<string> {
    return this.page.url()
  }

  /**
   * 
   * Returns an array of all open browser contexts. In a newly created browser, this will return zero browser contexts.
   */
  public async getContexts(): Promise<BrowserContext[]> {
    return this.browser.contexts();
  }

  /**
   * 
   * Returns all open pages in the context.
   */
  public async getPages(): Promise<Page[]> {
    return this.context.pages();
  }

  /**
   * Wait until timeout (as we know another name for it is "sleep" or "delay")
   * @param timeOutInsecond 
   */
  public async waitForTimeout(timeOutInsecond: number) {
    await this.page.waitForTimeout(timeOutInsecond * 1000)
  }

  /**
   * Waits for the main frame navigation and returns the main resource response. In case of multiple redirects, the
   * navigation will resolve with the response of the last redirect. In case of navigation to a different anchor or
   * navigation due to History API usage, the navigation will resolve with `null`.
   *
   * This resolves when the page navigates to a new URL or reloads. It is useful for when you run code which will indirectly
   * cause the page to navigate. e.g. The click target has an `onclick` handler that triggers navigation from a `setTimeout`.
   * Consider this example:
   *
   * > NOTE: Usage of the [History API](https://developer.mozilla.org/en-US/docs/Web/API/History_API) to change the URL is
   * considered a navigation.
   *
   * Shortcut for main frame's
   * [frame.waitForNavigation([options])](https://playwright.dev/docs/api/class-frame#frame-wait-for-navigation).
   * @param options
   */
  public async waitForNavigation(options: {
    timeout?: number;
    url?: string | RegExp | ((url: URL) => boolean);
    waitUntil?: "load" | "domcontentloaded" | "networkidle" | "commit";
  } = { timeout: this.navigateTimeout }): Promise<null | Response> {
    return await this.page.waitForNavigation(options)
  }

  /**
   * Returns when the required load state has been reached.
   *
   * This resolves when the page reaches a required load state, `load` by default. The navigation must have been committed
   * when this method is called. If current document has already reached the required state, resolves immediately.
   *
   * ```js
   * await page.click('button'); // Click triggers navigation.
   * await page.waitForLoadState(); // The promise resolves after 'load' event.
   * ```
   *
   * ```js
   * const [popup] = await Promise.all([
   *   // It is important to call waitForEvent before click to set up waiting.
   *   page.waitForEvent('popup'),
   *   // Click triggers a popup.
   *   page.locator('button').click(),
   * ])
   * await popup.waitForLoadState('domcontentloaded'); // The promise resolves after 'domcontentloaded' event.
   * console.log(await popup.title()); // Popup is ready to use.
   * ```
   *
   * Shortcut for main frame's
   * [frame.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-frame#frame-wait-for-load-state).
   * @param state Optional load state to wait for, defaults to `load`. If the state has been already reached while loading current document, the method resolves immediately. Can be one of:
   * - `'load'` - wait for the `load` event to be fired.
   * - `'domcontentloaded'` - wait for the `DOMContentLoaded` event to be fired.
   * - `'networkidle'` - wait until there are no network connections for at least `500` ms.
   * @param options
   */
  public async waitForLoadState(state?: "load" | "domcontentloaded" | "networkidle",
    timeout: number = this.actionTimeout
  ) {
    await this.page.waitForLoadState(state, { timeout: timeout })
  }

  /**
   * Waits for the matching request and returns it. See [waiting for event](https://playwright.dev/docs/events#waiting-for-event) for more details
   * about events.
   * @param urlOrPredicate Request URL string, regex or predicate receiving [Request] object.
   * @param options
   */
  public async waitForRequest(urlOrPredicate: string | RegExp | ((request: Request) => boolean | Promise<boolean>), timeout: number = this.actionTimeout
  ): Promise<Request> {
    return await this.page.waitForRequest(urlOrPredicate, { timeout: timeout })
  }

  /**
   * Returns the matched response. See [waiting for event](https://playwright.dev/docs/events#waiting-for-event) for more details about events.
   * @param urlOrPredicate Request URL string, regex or predicate receiving [Response] object. When a `baseURL` via the context options was provided and the passed URL is a path, it gets merged via the
   * [`new URL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL/URL) constructor.
   * @param options
   */
  public async waitForResponse(urlOrPredicate: string | RegExp | ((response: Response) => boolean | Promise<boolean>), timeout: number = this.actionTimeout): Promise<Response> {
    return await this.page.waitForResponse(urlOrPredicate, { timeout: timeout })
  }

  /**
   * Waits for the main frame to navigate to the given URL.
   * Shortcut for main frame's
   * [frame.waitForURL(url[, options])](https://playwright.dev/docs/api/class-frame#frame-wait-for-url).
   * @param url A glob pattern, regex pattern or predicate receiving [URL] to match while waiting for the navigation. Note that if the parameter is a string without wildcard characters, the method will wait for navigation to URL that is exactly equal to
   * the string.
   * @param options
   */
  public async waitForURL(url: string | RegExp | ((url: URL) => boolean), options: {
    timeout?: number;
    waitUntil?: "load" | "domcontentloaded" | "networkidle" | "commit";
  } = { timeout: this.actionTimeout }): Promise<void> {
    await this.page.waitForURL(url, options)
  }

  /**
   * Brings page to front (activates tab).
   * @param pageNumber 
   */
  public async bringToFront(pageNumber?: number): Promise<void> {
    let currentPages: Page[] = await this.getPages();
    if (pageNumber) {
      this.setPage(currentPages[pageNumber - 1]);
    }
    await this.page.bringToFront()
  }

  /**
   * In case this browser is obtained using
   * [browserType.launch([options])](https://playwright.dev/docs/api/class-browsertype#browser-type-launch), closes the
   * browser and all of its pages (if any were opened).
   *
   * In case this browser is connected to, clears all created contexts belonging to this browser and disconnects from the
   * browser server.
   *
   * The [Browser] object itself is considered to be disposed and cannot be used anymore.
   */
  public async closeBrowser(): Promise<void> {
    await this.browser.close();
  }

  /**
   * Closes the browser context. All the pages that belong to the browser context will be closed.
   *
   */
  public async closeContext(): Promise<void> {
    await this.context.close();
  }

  /**
   * If `runBeforeUnload` is `false`, does not run any unload handlers and waits for the page to be closed. If
   * `runBeforeUnload` is `true` the method will run unload handlers, but will **not** wait for the page to close.
   *
   * By default, `page.close()` **does not** run `beforeunload` handlers.
   *
   * > NOTE: if `runBeforeUnload` is passed as true, a `beforeunload` dialog might be summoned and should be handled manually
   * via [page.on('dialog')](https://playwright.dev/docs/api/class-page#page-event-dialog) event.
   * @param options
   */
  public async closePage(options?: {
    runBeforeUnload?: boolean;
  }): Promise<void> {
    await this.page.close(options);
  }

  /**
   * Close tab and bring to front another tab
   * @param pageNumber 
   */
  public async closeTabAndBringToFront(pageNumber: number): Promise<void> {
    let currentPages: Page[] = await this.getPages();
    let closePage = this.page;
    this.setPage(currentPages[pageNumber - 1]);
    await this.bringToFront();
    await closePage.close()
  }

  /**
 * > NOTE: Only works with Chromium browser's persistent context.
 *
 * Emitted when new background page is created in the context.
 *
 * ```js
 * const backgroundPage = await context.waitForEvent('backgroundpage');
 * ```
 *
 */
  public async waitForEventOnContext(event: 'backgroundpage', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when Browser context gets closed. This might happen because of one of the following:
   * - Browser context is closed.
   * - Browser application is closed or crashed.
   * - The [browser.close()](https://playwright.dev/docs/api/class-browser#browser-close) method was called.
   */
  public async waitForEventOnContext(event: 'close', optionsOrPredicate?: { predicate?: (browserContext: BrowserContext) => boolean | Promise<boolean>, timeout?: number } | ((browserContext: BrowserContext) => boolean | Promise<boolean>)): Promise<BrowserContext>;

  /**
   * The event is emitted when a new Page is created in the BrowserContext. The page may still be loading. The event will
   * also fire for popup pages. See also [page.on('popup')](https://playwright.dev/docs/api/class-page#page-event-popup) to
   * receive events about popups relevant to a specific page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * const [newPage] = await Promise.all([
   *   context.waitForEvent('page'),
   *   page.click('a[target=_blank]'),
   * ]);
   * console.log(await newPage.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async waitForEventOnContext(event: 'page', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when a request is issued from any pages created through this context. The [request] object is read-only. To only
   * listen for requests from a particular page, use
   * [page.on('request')](https://playwright.dev/docs/api/class-page#page-event-request).
   *
   * In order to intercept and mutate requests, see
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route)
   * or [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route).
   */
  public async waitForEventOnContext(event: 'request', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when a request fails, for example by timing out. To only listen for failed requests from a particular page, use
   * [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed).
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with
   * [browserContext.on('requestfinished')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-finished)
   * event and not with
   * [browserContext.on('requestfailed')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-failed).
   */
  public async waitForEventOnContext(event: 'requestfailed', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`. To listen for successful requests from a particular
   * page, use [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished).
   */
  public async waitForEventOnContext(event: 'requestfinished', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`. To listen for response events from a particular page, use
   * [page.on('response')](https://playwright.dev/docs/api/class-page#page-event-response).
   */
  public async waitForEventOnContext(event: 'response', optionsOrPredicate?: { predicate?: (response: Response) => boolean | Promise<boolean>, timeout?: number } | ((response: Response) => boolean | Promise<boolean>)): Promise<Response>;

  /**
   * > NOTE: Service workers are only supported on Chromium-based browsers.
   *
   * Emitted when new service worker is created in the context.
   */
  public async waitForEventOnContext(event: 'serviceworker', optionsOrPredicate?: { predicate?: (worker: Worker) => boolean | Promise<boolean>, timeout?: number } | ((worker: Worker) => boolean | Promise<boolean>)): Promise<Worker>;
  public async waitForEventOnContext(event: any, optionsOrPredicate: object = { timeout: this.actionTimeout }): Promise<Page | BrowserContext | Request | Response | Worker> {
    return await this.context.waitForEvent(event, optionsOrPredicate);
  };

  /**
   * Emitted when the page closes.
   */
  public async waitForEventOnPage(event: 'close', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when JavaScript within the page calls one of console API methods, e.g. `console.log` or `console.dir`. Also
   * emitted if the page throws an error or a warning.
   *
   * The arguments passed into `console.log` appear as arguments on the event handler.
   *
   * An example of handling `console` event:
   *
   * ```js
   * page.on('console', async msg => {
   *   const values = [];
   *   for (const arg of msg.args())
   *     values.push(await arg.jsonValue());
   *   console.log(...values);
   * });
   * await page.evaluate(() => console.log('hello', 5, {foo: 'bar'}));
   * ```
   *
   */
  public async waitForEventOnPage(event: 'console', optionsOrPredicate?: { predicate?: (consoleMessage: ConsoleMessage) => boolean | Promise<boolean>, timeout?: number } | ((consoleMessage: ConsoleMessage) => boolean | Promise<boolean>)): Promise<ConsoleMessage>;

  /**
   * Emitted when the page crashes. Browser pages might crash if they try to allocate too much memory. When the page crashes,
   * ongoing and subsequent operations will throw.
   *
   * The most common way to deal with crashes is to catch an exception:
   *
   * ```js
   * try {
   *   // Crash might happen during a click.
   *   await page.click('button');
   *   // Or while waiting for an event.
   *   await page.waitForEvent('popup');
   * } catch (e) {
   *   // When the page crashes, exception message contains 'crash'.
   * }
   * ```
   *
   */
  public async waitForEventOnPage(event: 'crash', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when a JavaScript dialog appears, such as `alert`, `prompt`, `confirm` or `beforeunload`. Listener **must**
   * either [dialog.accept([promptText])](https://playwright.dev/docs/api/class-dialog#dialog-accept) or
   * [dialog.dismiss()](https://playwright.dev/docs/api/class-dialog#dialog-dismiss) the dialog - otherwise the page will
   * [freeze](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop#never_blocking) waiting for the dialog, and
   * actions like click will never finish.
   *
   * ```js
   * page.on('dialog', dialog => {
   *   dialog.accept();
   * });
   * ```
   *
   * > NOTE: When no [page.on('dialog')](https://playwright.dev/docs/api/class-page#page-event-dialog) listeners are present,
   * all dialogs are automatically dismissed.
   */
  public async waitForEventOnPage(event: 'dialog', optionsOrPredicate?: { predicate?: (dialog: Dialog) => boolean | Promise<boolean>, timeout?: number } | ((dialog: Dialog) => boolean | Promise<boolean>)): Promise<Dialog>;

  /**
   * Emitted when the JavaScript [`DOMContentLoaded`](https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded)
   * event is dispatched.
   */
  public async waitForEventOnPage(event: 'domcontentloaded', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when attachment download started. User can access basic file operations on downloaded content via the passed
   * [Download] instance.
   */
  public async waitForEventOnPage(event: 'download', optionsOrPredicate?: { predicate?: (download: Download) => boolean | Promise<boolean>, timeout?: number } | ((download: Download) => boolean | Promise<boolean>)): Promise<Download>;

  /**
   * Emitted when a file chooser is supposed to appear, such as after clicking the  `<input type=file>`. Playwright can
   * respond to it via setting the input files using
   * [fileChooser.setFiles(files[, options])](https://playwright.dev/docs/api/class-filechooser#file-chooser-set-files) that
   * can be uploaded after that.
   *
   * ```js
   * page.on('filechooser', async (fileChooser) => {
   *   await fileChooser.setFiles('/tmp/myfile.pdf');
   * });
   * ```
   *
   */
  public async waitForEventOnPage(event: 'filechooser', optionsOrPredicate?: { predicate?: (fileChooser: FileChooser) => boolean | Promise<boolean>, timeout?: number } | ((fileChooser: FileChooser) => boolean | Promise<boolean>)): Promise<FileChooser>;

  /**
   * Emitted when a frame is attached.
   */
  public async waitForEventOnPage(event: 'frameattached', optionsOrPredicate?: { predicate?: (frame: Frame) => boolean | Promise<boolean>, timeout?: number } | ((frame: Frame) => boolean | Promise<boolean>)): Promise<Frame>;

  /**
   * Emitted when a frame is detached.
   */
  public async waitForEventOnPage(event: 'framedetached', optionsOrPredicate?: { predicate?: (frame: Frame) => boolean | Promise<boolean>, timeout?: number } | ((frame: Frame) => boolean | Promise<boolean>)): Promise<Frame>;

  /**
   * Emitted when a frame is navigated to a new url.
   */
  public async waitForEventOnPage(event: 'framenavigated', optionsOrPredicate?: { predicate?: (frame: Frame) => boolean | Promise<boolean>, timeout?: number } | ((frame: Frame) => boolean | Promise<boolean>)): Promise<Frame>;

  /**
   * Emitted when the JavaScript [`load`](https://developer.mozilla.org/en-US/docs/Web/Events/load) event is dispatched.
   */
  public async waitForEventOnPage(event: 'load', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when an uncaught exception happens within the page.
   *
   * ```js
   * // Log all uncaught errors to the terminal
   * page.on('pageerror', exception => {
   *   console.log(`Uncaught exception: "${exception}"`);
   * });
   *
   * // Navigate to a page with an exception.
   * await page.goto('data:text/html,<script>throw new Error("Test")</script>');
   * ```
   *
   */
  public async waitForEventOnPage(event: 'pageerror', optionsOrPredicate?: { predicate?: (error: Error) => boolean | Promise<boolean>, timeout?: number } | ((error: Error) => boolean | Promise<boolean>)): Promise<Error>;

  /**
   * Emitted when the page opens a new tab or window. This event is emitted in addition to the
   * [browserContext.on('page')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-page), but only
   * for popups relevant to this page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * // Note that Promise.all prevents a race condition
   * // between evaluating and waiting for the popup.
   * const [popup] = await Promise.all([
   *   // It is important to call waitForEvent first.
   *   page.waitForEvent('popup'),
   *   // Opens the popup.
   *   page.evaluate(() => window.open('https://example.com')),
   * ]);
   * console.log(await popup.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async waitForEventOnPage(event: 'popup', optionsOrPredicate?: { predicate?: (page: Page) => boolean | Promise<boolean>, timeout?: number } | ((page: Page) => boolean | Promise<boolean>)): Promise<Page>;

  /**
   * Emitted when a page issues a request. The [request] object is read-only. In order to intercept and mutate requests, see
   * [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route) or
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route).
   */
  public async waitForEventOnPage(event: 'request', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when a request fails, for example by timing out.
   *
   * ```js
   * page.on('requestfailed', request => {
   *   console.log(request.url() + ' ' + request.failure().errorText);
   * });
   * ```
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished) event
   * and not with [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed). A request
   * will only be considered failed when the client cannot get an HTTP response from the server, e.g. due to network error
   * net::ERR_FAILED.
   */
  public async waitForEventOnPage(event: 'requestfailed', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`.
   */
  public async waitForEventOnPage(event: 'requestfinished', optionsOrPredicate?: { predicate?: (request: Request) => boolean | Promise<boolean>, timeout?: number } | ((request: Request) => boolean | Promise<boolean>)): Promise<Request>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`.
   */
  public async waitForEventOnPage(event: 'response', optionsOrPredicate?: { predicate?: (response: Response) => boolean | Promise<boolean>, timeout?: number } | ((response: Response) => boolean | Promise<boolean>)): Promise<Response>;

  /**
   * Emitted when [WebSocket] request is sent.
   */
  public async waitForEventOnPage(event: 'websocket', optionsOrPredicate?: { predicate?: (webSocket: WebSocket) => boolean | Promise<boolean>, timeout?: number } | ((webSocket: WebSocket) => boolean | Promise<boolean>)): Promise<WebSocket>;

  /**
   * Emitted when a dedicated [WebWorker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API) is spawned by the
   * page.
   */
  public async waitForEventOnPage(event: 'worker', optionsOrPredicate?: { predicate?: (worker: Worker) => boolean | Promise<boolean>, timeout?: number } | ((worker: Worker) => boolean | Promise<boolean>)): Promise<Worker>;
  public async waitForEventOnPage(event: any, optionsOrPredicate: object = { timeout: this.actionTimeout }): Promise<Page | ConsoleMessage | Dialog | Download | FileChooser | Frame | Error | Request | Response | WebSocket | Worker> {
    return await this.page.waitForEvent(event, optionsOrPredicate);
  };

  /**
    * > NOTE: Only works with Chromium browser's persistent context.
    *
    * Emitted when new background page is created in the context.
    *
    * ```js
    * const backgroundPage = await context.waitForEvent('backgroundpage');
    * ```
    *
    */
  public async turnOnEventOnContext(event: 'backgroundpage', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when Browser context gets closed. This might happen because of one of the following:
   * - Browser context is closed.
   * - Browser application is closed or crashed.
   * - The [browser.close()](https://playwright.dev/docs/api/class-browser#browser-close) method was called.
   */
  public async turnOnEventOnContext(event: 'close', listener: (browserContext: BrowserContext) => void): Promise<void>;

  /**
   * The event is emitted when a new Page is created in the BrowserContext. The page may still be loading. The event will
   * also fire for popup pages. See also [page.on('popup')](https://playwright.dev/docs/api/class-page#page-event-popup) to
   * receive events about popups relevant to a specific page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * const [newPage] = await Promise.all([
   *   context.waitForEvent('page'),
   *   page.click('a[target=_blank]'),
   * ]);
   * console.log(await newPage.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async turnOnEventOnContext(event: 'page', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a request is issued from any pages created through this context. The [request] object is read-only. To only
   * listen for requests from a particular page, use
   * [page.on('request')](https://playwright.dev/docs/api/class-page#page-event-request).
   *
   * In order to intercept and mutate requests, see
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route)
   * or [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route).
   */
  public async turnOnEventOnContext(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request fails, for example by timing out. To only listen for failed requests from a particular page, use
   * [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed).
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with
   * [browserContext.on('requestfinished')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-finished)
   * event and not with
   * [browserContext.on('requestfailed')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-failed).
   */
  public async turnOnEventOnContext(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`. To listen for successful requests from a particular
   * page, use [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished).
   */
  public async turnOnEventOnContext(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`. To listen for response events from a particular page, use
   * [page.on('response')](https://playwright.dev/docs/api/class-page#page-event-response).
   */
  public async turnOnEventOnContext(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * > NOTE: Service workers are only supported on Chromium-based browsers.
   *
   * Emitted when new service worker is created in the context.
   */
  public async turnOnEventOnContext(event: 'serviceworker', listener: (worker: Worker) => void): Promise<void>;
  public async turnOnEventOnContext(event: any, listener: any): Promise<void> {
    await this.context.on(event, listener);
  }

  /**
   * Emitted when the page closes.
   */
  public async turnOnEventOnPage(event: "close", listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when JavaScript within the page calls one of console API methods, e.g. `console.log` or `console.dir`. Also
   * emitted if the page throws an error or a warning.
   *
   * The arguments passed into `console.log` appear as arguments on the event handler.
   *
   * An example of handling `console` event:
   *
   * ```js
   * page.on('console', async msg => {
   *   const values = [];
   *   for (const arg of msg.args())
   *     values.push(await arg.jsonValue());
   *   console.log(...values);
   * });
   * await page.evaluate(() => console.log('hello', 5, {foo: 'bar'}));
   * ```
   *
   */
  public async turnOnEventOnPage(event: "console", listener: (consoleMessage: ConsoleMessage) => void): Promise<void>;

  /**
   * Emitted when the page crashes. Browser pages might crash if they try to allocate too much memory. When the page crashes,
   * ongoing and subsequent operations will throw.
   *
   * The most common way to deal with crashes is to catch an exception:
   *
   * ```js
   * try {
   *   // Crash might happen during a click.
   *   await page.click('button');
   *   // Or while waiting for an event.
   *   await page.waitForEvent('popup');
   * } catch (e) {
   *   // When the page crashes, exception message contains 'crash'.
   * }
   * ```
   *
   */
  public async turnOnEventOnPage(event: "crash", listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a JavaScript dialog appears, such as `alert`, `prompt`, `confirm` or `beforeunload`. Listener **must**
   * either [dialog.accept([promptText])](https://playwright.dev/docs/api/class-dialog#dialog-accept) or
   * [dialog.dismiss()](https://playwright.dev/docs/api/class-dialog#dialog-dismiss) the dialog - otherwise the page will
   * [freeze](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop#never_blocking) waiting for the dialog, and
   * actions like click will never finish.
   *
   * ```js
   * page.on('dialog', dialog => {
   *   dialog.accept();
   * });
   * ```
   *
   * > NOTE: When no [page.on('dialog')](https://playwright.dev/docs/api/class-page#page-event-dialog) listeners are present,
   * all dialogs are automatically dismissed.
   */
  public async turnOnEventOnPage(event: "dialog", listener: (dialog: Dialog) => void): Promise<void>;

  /**
   * Emitted when the JavaScript [`DOMContentLoaded`](https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded)
   * event is dispatched.
   */
  public async turnOnEventOnPage(event: "domcontentloaded", listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when attachment download started. User can access basic file operations on downloaded content via the passed
   * [Download] instance.
   */
  public async turnOnEventOnPage(event: "download", listener: (download: Download) => void): Promise<void>;

  /**
   * Emitted when a file chooser is supposed to appear, such as after clicking the  `<input type=file>`. Playwright can
   * respond to it via setting the input files using
   * [fileChooser.setFiles(files[, options])](https://playwright.dev/docs/api/class-filechooser#file-chooser-set-files) that
   * can be uploaded after that.
   *
   * ```js
   * page.on('filechooser', async (fileChooser) => {
   *   await fileChooser.setFiles('/tmp/myfile.pdf');
   * });
   * ```
   *
   */
  public async turnOnEventOnPage(event: "filechooser", listener: (fileChooser: FileChooser) => void): Promise<void>;

  /**
   * Emitted when a frame is attached.
   */
  public async turnOnEventOnPage(event: "frameattached", listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when a frame is detached.
   */
  public async turnOnEventOnPage(event: "framedetached", listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when a frame is navigated to a new url.
   */
  public async turnOnEventOnPage(event: "framenavigated", listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when the JavaScript [`load`](https://developer.mozilla.org/en-US/docs/Web/Events/load) event is dispatched.
   */
  public async turnOnEventOnPage(event: "load", listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when an uncaught exception happens within the page.
   *
   * ```js
   * // Log all uncaught errors to the terminal
   * page.on('pageerror', exception => {
   *   console.log(`Uncaught exception: "${exception}"`);
   * });
   *
   * // Navigate to a page with an exception.
   * await page.goto('data:text/html,<script>throw new Error("Test")</script>');
   * ```
   *
   */
  public async turnOnEventOnPage(event: "pageerror", listener: (error: Error) => void): Promise<void>;

  /**
   * Emitted when the page opens a new tab or window. This event is emitted in addition to the
   * [browserContext.on('page')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-page), but only
   * for popups relevant to this page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * // Note that Promise.all prevents a race condition
   * // between evaluating and waiting for the popup.
   * const [popup] = await Promise.all([
   *   // It is important to call waitForEvent first.
   *   page.waitForEvent('popup'),
   *   // Opens the popup.
   *   page.evaluate(() => window.open('https://example.com')),
   * ]);
   * console.log(await popup.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async turnOnEventOnPage(event: "popup", listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a page issues a request. The [request] object is read-only. In order to intercept and mutate requests, see
   * [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route) or
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route).
   */
  public async turnOnEventOnPage(event: "request", listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request fails, for example by timing out.
   *
   * ```js
   * page.on('requestfailed', request => {
   *   console.log(request.url() + ' ' + request.failure().errorText);
   * });
   * ```
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished) event
   * and not with [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed). A request
   * will only be considered failed when the client cannot get an HTTP response from the server, e.g. due to network error
   * net::ERR_FAILED.
   */
  public async turnOnEventOnPage(event: "requestfailed", listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`.
   */
  public async turnOnEventOnPage(event: "requestfinished", listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`.
   */
  public async turnOnEventOnPage(event: "response", listener: (response: Response) => void): Promise<void>;

  /**
   * Emitted when [WebSocket] request is sent.
   */
  public async turnOnEventOnPage(event: "websocket", listener: (webSocket: WebSocket) => void): Promise<void>;

  /**
   * Emitted when a dedicated [WebWorker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API) is spawned by the
   * page.
   */
  public async turnOnEventOnPage(event: "worker", listener: (worker: Worker) => void): Promise<void>;
  public async turnOnEventOnPage(event: any, listener: any): Promise<void> {
    await this.page.on(event, listener);
  }

  /**
  * Removes an event listener added by `on` or `addListener`.
  */
  public async turnOffEventOnContext(event: 'backgroundpage', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'close', listener: (browserContext: BrowserContext) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'page', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnContext(event: 'serviceworker', listener: (worker: Worker) => void): Promise<void>;
  public async turnOffEventOnContext(event: any, listener: any): Promise<void> {
    await this.context.off(event, listener);
  }

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'close', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'console', listener: (consoleMessage: ConsoleMessage) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'crash', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'dialog', listener: (dialog: Dialog) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'domcontentloaded', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'download', listener: (download: Download) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'filechooser', listener: (fileChooser: FileChooser) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'frameattached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'framedetached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'framenavigated', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'load', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'pageerror', listener: (error: Error) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'popup', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'websocket', listener: (webSocket: WebSocket) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async turnOffEventOnPage(event: 'worker', listener: (worker: Worker) => void): Promise<void>;
  public async turnOffEventOnPage(event: any, listener: any): Promise<void> {
    await this.page.off(event, listener);
  }

  /**
   * > NOTE: Only works with Chromium browser's persistent context.
   *
   * Emitted when new background page is created in the context.
   *
   * ```js
   * const backgroundPage = await context.waitForEvent('backgroundpage');
   * ```
   *
   */
  public async turnOnEventOnceOnContext(event: 'backgroundpage', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when Browser context gets closed. This might happen because of one of the following:
   * - Browser context is closed.
   * - Browser application is closed or crashed.
   * - The [browser.close()](https://playwright.dev/docs/api/class-browser#browser-close) method was called.
   */
  public async turnOnEventOnceOnContext(event: 'close', listener: (browserContext: BrowserContext) => void): Promise<void>;

  /**
   * The event is emitted when a new Page is created in the BrowserContext. The page may still be loading. The event will
   * also fire for popup pages. See also [page.on('popup')](https://playwright.dev/docs/api/class-page#page-event-popup) to
   * receive events about popups relevant to a specific page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * const [newPage] = await Promise.all([
   *   context.waitForEvent('page'),
   *   page.click('a[target=_blank]'),
   * ]);
   * console.log(await newPage.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async turnOnEventOnceOnContext(event: 'page', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a request is issued from any pages created through this context. The [request] object is read-only. To only
   * listen for requests from a particular page, use
   * [page.on('request')](https://playwright.dev/docs/api/class-page#page-event-request).
   *
   * In order to intercept and mutate requests, see
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route)
   * or [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route).
   */
  public async turnOnEventOnceOnContext(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request fails, for example by timing out. To only listen for failed requests from a particular page, use
   * [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed).
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with
   * [browserContext.on('requestfinished')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-finished)
   * event and not with
   * [browserContext.on('requestfailed')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-failed).
   */
  public async turnOnEventOnceOnContext(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`. To listen for successful requests from a particular
   * page, use [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished).
   */
  public async turnOnEventOnceOnContext(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`. To listen for response events from a particular page, use
   * [page.on('response')](https://playwright.dev/docs/api/class-page#page-event-response).
   */
  public async turnOnEventOnceOnContext(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * > NOTE: Service workers are only supported on Chromium-based browsers.
   *
   * Emitted when new service worker is created in the context.
   */
  public async turnOnEventOnceOnContext(event: 'serviceworker', listener: (worker: Worker) => void): Promise<void>;
  public async turnOnEventOnceOnContext(event: any, listener: any): Promise<void> {
    await this.context.on(event, listener);
  }


  /**
   * Adds an event listener that will be automatically removed after it is triggered once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'close', listener: (page: Page) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'console', listener: (consoleMessage: ConsoleMessage) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'crash', listener: (page: Page) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'dialog', listener: (dialog: Dialog) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'domcontentloaded', listener: (page: Page) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'download', listener: (download: Download) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'filechooser', listener: (fileChooser: FileChooser) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'frameattached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'framedetached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'framenavigated', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'load', listener: (page: Page) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'pageerror', listener: (error: Error) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'popup', listener: (page: Page) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'websocket', listener: (webSocket: WebSocket) => void): Promise<void>;

  /**
   * Adds an event listener that will be automatically removed after it is triggered public async once. See `addListener` for more information about this event.
   */
  public async turnOnEventOnceOnPage(event: 'worker', listener: (worker: Worker) => void): Promise<void>;
  public async turnOnEventOnceOnPage(event: any, listener: any): Promise<void> {
    await this.page.once(event, listener);
  }


  /**
   * > NOTE: Only works with Chromium browser's persistent context.
   *
   * Emitted when new background page is created in the context.
   *
   * ```js
   * const backgroundPage = await context.waitForEvent('backgroundpage');
   * ```
   *
   */
  public async addListenerOnContext(event: 'backgroundpage', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when Browser context gets closed. This might happen because of one of the following:
   * - Browser context is closed.
   * - Browser application is closed or crashed.
   * - The [browser.close()](https://playwright.dev/docs/api/class-browser#browser-close) method was called.
   */
  public async addListenerOnContext(event: 'close', listener: (browserContext: BrowserContext) => void): Promise<void>;

  /**
   * The event is emitted when a new Page is created in the BrowserContext. The page may still be loading. The event will
   * also fire for popup pages. See also [page.on('popup')](https://playwright.dev/docs/api/class-page#page-event-popup) to
   * receive events about popups relevant to a specific page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * const [newPage] = await Promise.all([
   *   context.waitForEvent('page'),
   *   page.click('a[target=_blank]'),
   * ]);
   * console.log(await newPage.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async addListenerOnContext(event: 'page', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a request is issued from any pages created through this context. The [request] object is read-only. To only
   * listen for requests from a particular page, use
   * [page.on('request')](https://playwright.dev/docs/api/class-page#page-event-request).
   *
   * In order to intercept and mutate requests, see
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route)
   * or [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route).
   */
  public async addListenerOnContext(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request fails, for example by timing out. To only listen for failed requests from a particular page, use
   * [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed).
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with
   * [browserContext.on('requestfinished')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-finished)
   * event and not with
   * [browserContext.on('requestfailed')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-request-failed).
   */
  public async addListenerOnContext(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`. To listen for successful requests from a particular
   * page, use [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished).
   */
  public async addListenerOnContext(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`. To listen for response events from a particular page, use
   * [page.on('response')](https://playwright.dev/docs/api/class-page#page-event-response).
   */
  public async addListenerOnContext(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * > NOTE: Service workers are only supported on Chromium-based browsers.
   *
   * Emitted when new service worker is created in the context.
   */
  public async addListenerOnContext(event: 'serviceworker', listener: (worker: Worker) => void): Promise<void>;
  public async addListenerOnContext(event: any, listener: any): Promise<void> {
    await this.context.addListener(event, listener);
  }


  /**
   * Emitted when the page closes.
   */
  public async addListenerOnPage(event: 'close', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when JavaScript within the page calls one of console API methods, e.g. `console.log` or `console.dir`. Also
   * emitted if the page throws an error or a warning.
   *
   * The arguments passed into `console.log` appear as arguments on the event handler.
   *
   * An example of handling `console` event:
   *
   * ```js
   * page.on('console', async msg => {
   *   const values = [];
   *   for (const arg of msg.args())
   *     values.push(await arg.jsonValue());
   *   console.log(...values);
   * });
   * await page.evaluate(() => console.log('hello', 5, {foo: 'bar'}));
   * ```
   *
   */
  public async addListenerOnPage(event: 'console', listener: (consoleMessage: ConsoleMessage) => void): Promise<void>;

  /**
   * Emitted when the page crashes. Browser pages might crash if they try to allocate too much memory. When the page crashes,
   * ongoing and subsequent operations will throw.
   *
   * The most common way to deal with crashes is to catch an exception:
   *
   * ```js
   * try {
   *   // Crash might happen during a click.
   *   await page.click('button');
   *   // Or while waiting for an event.
   *   await page.waitForEvent('popup');
   * } catch (e) {
   *   // When the page crashes, exception message contains 'crash'.
   * }
   * ```
   *
   */
  public async addListenerOnPage(event: 'crash', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a JavaScript dialog appears, such as `alert`, `prompt`, `confirm` or `beforeunload`. Listener **must**
   * either [dialog.accept([promptText])](https://playwright.dev/docs/api/class-dialog#dialog-accept) or
   * [dialog.dismiss()](https://playwright.dev/docs/api/class-dialog#dialog-dismiss) the dialog - otherwise the page will
   * [freeze](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop#never_blocking) waiting for the dialog, and
   * actions like click will never finish.
   *
   * ```js
   * page.on('dialog', dialog => {
   *   dialog.accept();
   * });
   * ```
   *
   * > NOTE: When no [page.on('dialog')](https://playwright.dev/docs/api/class-page#page-event-dialog) listeners are present,
   * all dialogs are automatically dismissed.
   */
  public async addListenerOnPage(event: 'dialog', listener: (dialog: Dialog) => void): Promise<void>;

  /**
   * Emitted when the JavaScript [`DOMContentLoaded`](https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded)
   * event is dispatched.
   */
  public async addListenerOnPage(event: 'domcontentloaded', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when attachment download started. User can access basic file operations on downloaded content via the passed
   * [Download] instance.
   */
  public async addListenerOnPage(event: 'download', listener: (download: Download) => void): Promise<void>;

  /**
   * Emitted when a file chooser is supposed to appear, such as after clicking the  `<input type=file>`. Playwright can
   * respond to it via setting the input files using
   * [fileChooser.setFiles(files[, options])](https://playwright.dev/docs/api/class-filechooser#file-chooser-set-files) that
   * can be uploaded after that.
   *
   * ```js
   * page.on('filechooser', async (fileChooser) => {
   *   await fileChooser.setFiles('/tmp/myfile.pdf');
   * });
   * ```
   *
   */
  public async addListenerOnPage(event: 'filechooser', listener: (fileChooser: FileChooser) => void): Promise<void>;

  /**
   * Emitted when a frame is attached.
   */
  public async addListenerOnPage(event: 'frameattached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when a frame is detached.
   */
  public async addListenerOnPage(event: 'framedetached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when a frame is navigated to a new url.
   */
  public async addListenerOnPage(event: 'framenavigated', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Emitted when the JavaScript [`load`](https://developer.mozilla.org/en-US/docs/Web/Events/load) event is dispatched.
   */
  public async addListenerOnPage(event: 'load', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when an uncaught exception happens within the page.
   *
   * ```js
   * // Log all uncaught errors to the terminal
   * page.on('pageerror', exception => {
   *   console.log(`Uncaught exception: "${exception}"`);
   * });
   *
   * // Navigate to a page with an exception.
   * await page.goto('data:text/html,<script>throw new Error("Test")</script>');
   * ```
   *
   */
  public async addListenerOnPage(event: 'pageerror', listener: (error: Error) => void): Promise<void>;

  /**
   * Emitted when the page opens a new tab or window. This event is emitted in addition to the
   * [browserContext.on('page')](https://playwright.dev/docs/api/class-browsercontext#browser-context-event-page), but only
   * for popups relevant to this page.
   *
   * The earliest moment that page is available is when it has navigated to the initial url. For example, when opening a
   * popup with `window.open('http://example.com')`, this event will fire when the network request to "http://example.com" is
   * done and its response has started loading in the popup.
   *
   * ```js
   * // Note that Promise.all prevents a race condition
   * // between evaluating and waiting for the popup.
   * const [popup] = await Promise.all([
   *   // It is important to call waitForEvent first.
   *   page.waitForEvent('popup'),
   *   // Opens the popup.
   *   page.evaluate(() => window.open('https://example.com')),
   * ]);
   * console.log(await popup.evaluate('location.href'));
   * ```
   *
   * > NOTE: Use
   * [page.waitForLoadState([state, options])](https://playwright.dev/docs/api/class-page#page-wait-for-load-state) to wait
   * until the page gets to a particular state (you should not need it in most cases).
   */
  public async addListenerOnPage(event: 'popup', listener: (page: Page) => void): Promise<void>;

  /**
   * Emitted when a page issues a request. The [request] object is read-only. In order to intercept and mutate requests, see
   * [page.route(url, handler[, options])](https://playwright.dev/docs/api/class-page#page-route) or
   * [browserContext.route(url, handler[, options])](https://playwright.dev/docs/api/class-browsercontext#browser-context-route).
   */
  public async addListenerOnPage(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request fails, for example by timing out.
   *
   * ```js
   * page.on('requestfailed', request => {
   *   console.log(request.url() + ' ' + request.failure().errorText);
   * });
   * ```
   *
   * > NOTE: HTTP Error responses, such as 404 or 503, are still successful responses from HTTP standpoint, so request will
   * complete with [page.on('requestfinished')](https://playwright.dev/docs/api/class-page#page-event-request-finished) event
   * and not with [page.on('requestfailed')](https://playwright.dev/docs/api/class-page#page-event-request-failed). A request
   * will only be considered failed when the client cannot get an HTTP response from the server, e.g. due to network error
   * net::ERR_FAILED.
   */
  public async addListenerOnPage(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when a request finishes successfully after downloading the response body. For a successful response, the
   * sequence of events is `request`, `response` and `requestfinished`.
   */
  public async addListenerOnPage(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Emitted when [response] status and headers are received for a request. For a successful response, the sequence of events
   * is `request`, `response` and `requestfinished`.
   */
  public async addListenerOnPage(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Emitted when [WebSocket] request is sent.
   */
  public async addListenerOnPage(event: 'websocket', listener: (webSocket: WebSocket) => void): Promise<void>;

  /**
   * Emitted when a dedicated [WebWorker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API) is spawned by the
   * page.
   */
  public async addListenerOnPage(event: 'worker', listener: (worker: Worker) => void): Promise<void>;
  public async addListenerOnPage(event: any, listener: any): Promise<void> {
    await this.page.addListener(event, listener);
  }

  /**
    * Removes an event listener added by `on` or `addListener`.
    */
  public async removeListenerOnContext(event: 'backgroundpage', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'close', listener: (browserContext: BrowserContext) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'page', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnContext(event: 'serviceworker', listener: (worker: Worker) => void): Promise<void>;
  public async removeListenerOnContext(event: any, listener: any): Promise<void> {
    await this.context.removeListener(event, listener);
  }

  /**
 * Removes an event listener added by `on` or `addListener`.
 */
  public async removeListenerOnPage(event: 'close', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'console', listener: (consoleMessage: ConsoleMessage) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'crash', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'dialog', listener: (dialog: Dialog) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'domcontentloaded', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'download', listener: (download: Download) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'filechooser', listener: (fileChooser: FileChooser) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'frameattached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'framedetached', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'framenavigated', listener: (frame: Frame) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'load', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'pageerror', listener: (error: Error) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'popup', listener: (page: Page) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'request', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'requestfailed', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'requestfinished', listener: (request: Request) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'response', listener: (response: Response) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'websocket', listener: (webSocket: WebSocket) => void): Promise<void>;

  /**
   * Removes an event listener added by `on` or `addListener`.
   */
  public async removeListenerOnPage(event: 'worker', listener: (worker: Worker) => void): Promise<void>;
  public async removeListenerOnPage(event: any, listener: any): Promise<void> {
    await this.page.removeListener(event, listener);
  }

  /**
  * Returns when the `pageFunction` returns a truthy value. It resolves to a JSHandle of the truthy value.
  *
  * The
  * [page.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-page#page-wait-for-function)
  * can be used to observe viewport size change:
  *
  * ```js
  * const { webkit } = require('playwright');  // Or 'chromium' or 'firefox'.
  *
  * (async () => {
  *   const browser = await webkit.launch();
  *   const page = await browser.newPage();
  *   const watchDog = page.waitForFunction(() => window.innerWidth < 100);
  *   await page.setViewportSize({width: 50, height: 50});
  *   await watchDog;
  *   await browser.close();
  * })();
  * ```
  *
  * To pass an argument to the predicate of
  * [page.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-page#page-wait-for-function)
  * function:
  *
  * ```js
  * const selector = '.foo';
  * await page.waitForFunction(selector => !!document.querySelector(selector), selector);
  * ```
  *
  * Shortcut for main frame's
  * [frame.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-frame#frame-wait-for-function).
  * @param pageFunction Function to be evaluated in the page context.
  * @param arg Optional argument to pass to `pageFunction`.
  * @param options
  */
  public async waitForFunction<R, Arg>(pageFunction: PageFunction<Arg, R>, arg: Arg, options?: PageWaitForFunctionOptions): Promise<SmartHandle<R>>;

  /**
  * Returns when the `pageFunction` returns a truthy value. It resolves to a JSHandle of the truthy value.
  *
  * The
  * [page.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-page#page-wait-for-function)
  * can be used to observe viewport size change:
  *
  * ```js
  * const { webkit } = require('playwright');  // Or 'chromium' or 'firefox'.
  *
  * (async () => {
  *   const browser = await webkit.launch();
  *   const page = await browser.newPage();
  *   const watchDog = page.waitForFunction(() => window.innerWidth < 100);
  *   await page.setViewportSize({width: 50, height: 50});
  *   await watchDog;
  *   await browser.close();
  * })();
  * ```
  *
  * To pass an argument to the predicate of
  * [page.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-page#page-wait-for-function)
  * function:
  *
  * ```js
  * const selector = '.foo';
  * await page.waitForFunction(selector => !!document.querySelector(selector), selector);
  * ```
  *
  * Shortcut for main frame's
  * [frame.waitForFunction(pageFunction[, arg, options])](https://playwright.dev/docs/api/class-frame#frame-wait-for-function).
  * @param pageFunction Function to be evaluated in the page context.
  * @param arg Optional argument to pass to `pageFunction`.
  * @param options
  */
  public async waitForFunction<R>(pageFunction: PageFunction<void, R>, arg?: any, options?: PageWaitForFunctionOptions): Promise<SmartHandle<R>>;
  public async waitForFunction(pageFunction: any, arg?: any, options?: PageWaitForFunctionOptions): Promise<any> {
    try {
      return await this.page.waitForFunction(pageFunction, arg, options)
    } catch (error) {
      console.log(error.message)
    }
  }

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector<K extends keyof HTMLElementTagNameMap>(selector: K, options?: PageWaitForSelectorOptionsNotHidden): Promise<ElementHandleForTag<K>>;

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector(selector: string, options?: PageWaitForSelectorOptionsNotHidden): Promise<ElementHandle<SVGElement | HTMLElement>>;

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector<K extends keyof HTMLElementTagNameMap>(selector: K, options: PageWaitForSelectorOptions): Promise<ElementHandleForTag<K> | null>;

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector(selector: string, options?: PageWaitForSelectorOptionsNotHidden): Promise<ElementHandle<SVGElement | HTMLElement>>;

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector<K extends keyof HTMLElementTagNameMap>(selector: K, options: PageWaitForSelectorOptions): Promise<ElementHandleForTag<K> | null>;

  /**
   * Returns when element specified by selector satisfies `state` option. Returns `null` if waiting for `hidden` or
   * `detached`.
   *
   * > NOTE: Playwright automatically waits for element to be ready before performing an action. Using [Locator] objects and
   * web-first assertions make the code wait-for-selector-free.
   *
   * Wait for the `selector` to satisfy `state` option (either appear/disappear from dom, or become visible/hidden). If at
   * the moment of calling the method `selector` already satisfies the condition, the method will return immediately. If the
   * selector doesn't satisfy the condition for the `timeout` milliseconds, the function will throw.
   *
   * This method works across navigations:
   *
   * ```js
   * const { chromium } = require('playwright');  // Or 'firefox' or 'webkit'.
   *
   * (async () => {
   *   const browser = await chromium.launch();
   *   const page = await browser.newPage();
   *   for (let currentURL of ['https://google.com', 'https://bbc.com']) {
   *     await page.goto(currentURL);
   *     const element = await page.waitForSelector('img');
   *     console.log('Loaded image: ' + await element.getAttribute('src'));
   *   }
   *   await browser.close();
   * })();
   * ```
   *
   * @param selector A selector to query for. See [working with selectors](https://playwright.dev/docs/selectors) for more details.
   * @param options
   */
  public async waitForSelector(selector: string, options: PageWaitForSelectorOptions): Promise<null | ElementHandle<SVGElement | HTMLElement>>;
  public async waitForSelector(selector: any, options?: any): Promise<any> {
    try {
      return await this.page.waitForSelector(selector, options)
    } catch (error) {
      console.log(error.message)
    }
  }


  // add custom methods here
  // If you need to access other helpers
  // use: this.helpers['helperName']

}

export = BrowserWrapper;
